from Virtus.model import DispatcherModel
import pyomo.environ as pyo
from pyomo.core import Var, Objective
from pyomo.opt import SolverStatus, TerminationCondition

class Solver:

    def __init__(self, data):
        # TODO: change pods and timestamps
        self.pods = data[None]['n_pods'][None]
        self.timestamps = data[None]['n_timestamps'][None]
        self.powers_result = [[0 for x in range(self.timestamps)] for y in range(self.pods)]
        self.model = DispatcherModel(data)


    def resolve(self):
        instance = self.model.model_create_instance()
        solver = pyo.SolverFactory('gurobi', solver_io="python")
        #solver = pyo.SolverFactory('cplex', executable='/Applications/CPLEX_Studio129/cplex/bin/x86-64_osx/cplex')
        #solver.options['optimalitytarget'] = 3
        result = solver.solve(instance, tee=True)
        instance.pprint()

        # Check the results and extract the values if the solution is optimal
        if (result.solver.status == SolverStatus.ok) and (
                result.solver.termination_condition == TerminationCondition.optimal):

            # instance.pprint()

            for v in instance.component_objects(Var, active=True):
                # print ("Variable",v)
                varobject = getattr(instance, str(v))
                if v.name == 'pods_power':
                    for index in varobject:
                        self.powers_result[index[0]][index[1]] = varobject[index].value

            for v in instance.component_objects(Objective, active=True):
                self.solution_value = v.expr()
            #print("%.2f" % self.solution_value)

            #return self

        elif (result.solver.termination_condition == TerminationCondition.infeasible):
            print('############################ INFEASIBLE MODEL ############################')
            raise Exception('############################ INFEASIBLE MODEL ############################')
        else:
            print('############################ SOMETHING WENT WRONG ############################')
            print("Solver Status: ", result.solver.status)
            raise Exception(
                "############################ INFEASIBLE MODEL ############################\nSolver Status: ",
                result.solver.status)

    def powers_result(self):
        return self.powers_result

    def solution_value(self):
        return self.solution_value
