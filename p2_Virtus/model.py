import pyomo.environ as pyo

class DispatcherModel :

    model = pyo.AbstractModel()

    def __model_parameters_setup(self):
        # Number of timestamps. By default it is 96
        self.model.n_timestamps = pyo.Param(domain=pyo.PositiveIntegers, default=96, mutable=True)

        # TODO: this model of the flexibility band only works with some PODs. For PODs with different behaviour other flexibility band models should be added (e.g.: model of the battery).
        # Note: This model is inferred by Libra architecture

        # Number of the pods of the model (with this flexibility band model)
        self.model.n_pods = pyo.Param(domain=pyo.PositiveIntegers, default=2, mutable=True)

        # Number of parameters of the flexibility
        # Default is 3: cost/kW, min power, max power (for each flexibility band)
        self.model.n_flex_param = pyo.Param(domain=pyo.PositiveIntegers, default=3, mutable=True)

        # Maximum number of flexibility bands for PODs. Useful for sizing model variables
        self.model.n_flex_max = pyo.Param(domain=pyo.PositiveIntegers, default=3, mutable=True)

        # Declaration of implicit parameters
        self.__model_implicit_parameters_setup()

        # Declaration of flexibility bands (to be override when realizing the model) for each POD
        self.model.flexibility_bands = pyo.Param(self.model.P, self.model.FM, self.model.FP, default=0)

        # Declaration of PODs' baselines
        self.model.baselines = pyo.Param(self.model.P, self.model.T, domain=pyo.Reals, default=0)

    def __model_implicit_parameters_setup(self):
        # Timestamps
        self.model.T = pyo.RangeSet(0, self.model.n_timestamps-1)
        # PODs
        self.model.P = pyo.RangeSet(0, self.model.n_pods-1)
        # Flexibility bands max
        self.model.FM = pyo.RangeSet(self.model.n_flex_max)
        # Flexibility band parameters
        self.model.FP = pyo.RangeSet(self.model.n_flex_param)

    def __model_variable_setup(self):
        # Power of each POD at each timestamp
        self.model.pods_power = pyo.Var(self.model.P, self.model.T, domain=pyo.Reals)

        # Flexibility band chosen for each POD at each timestamp
        self.model.pods_flexibility_band = pyo.Var(self.model.P, self.model.T, self.model.FM, domain=pyo.Binary)

    def __model_constraint_setup(self):
        # Flexibility lower bound on powers
        def flex_lb(m, p, t, f):
            return m.pods_power[p, t]*m.pods_flexibility_band[p, t, f] >= m.flexibility_bands[p, f, 2]*m.pods_flexibility_band[p, t, f]
            #return m.pods_power[p, t] >= m.flexibility_bands[p, pyo.value(m.pods_flexibility_band[p, t]), 2]
        self.model.flex_lb = pyo.Constraint(self.model.P, self.model.T, self.model.FM, rule=flex_lb)

        # Flexibility upper bound on powers
        def flex_ub(m, p, t, f):
            return m.pods_power[p, t] * m.pods_flexibility_band[p, t, f] <= m.flexibility_bands[p, f, 3] * \
                   m.pods_flexibility_band[p, t, f]
            #return m.pods_power[p, t] <= m.flexibility_bands[p, m.pods_flexibility_band[p, t], 3]
        self.model.flex_ub = pyo.Constraint(self.model.P, self.model.T, self.model.FM, rule=flex_ub)

        def flex_selection(m, p, t):
            return sum(m.pods_flexibility_band[p, t, f] for f in m.FM) == 1
        self.model.flex_selection = pyo.Constraint(self.model.P, self.model.T, rule=flex_selection)

        # Power balance constraint
        # TODO: update with external load?
        def power_balance(m, t):
            return sum(m.pods_power[x, t] for x in m.P) == 0
        self.model.power_balance = pyo.Constraint(self.model.T, rule=power_balance)

    def __model_obj_func_setup(self):
        # TODO: change obj function?
        def objective_function(m):
            return sum(m.pods_power[x, t] * m.flexibility_bands[x, f, 1]*m.pods_flexibility_band[x, t, f] for x in m.P for t in m.T for f in m.FM)
            #return sum(m.pods_power[x, t]*m.flexibility_bands[x, m.pods_flexibility_band[x, t], 1] for x in m.P for t in m.T)
        self.model.objective_function = pyo.Objective(rule=objective_function, sense=pyo.minimize)

    def __init__(self, data):
        self.__data_dictionary_creation(data)
        self.__model_parameters_setup()
        self.__model_variable_setup()
        self.__model_constraint_setup()
        self.__model_obj_func_setup()

    def __data_dictionary_creation(self, data):
        # TODO: parse data object from BDE
        # The parameter 'data' should be the message from the BDE. Here it should be parsed to match the following form:
        # { None: {
        #   'n_pods': { None: 100 },
        #   'n_flex_param': { None: 3 },
        #   'n_flex_max': { None: 3 },
        #   'baselines': { (0,0): 0.2, (0,1): 0.4, ... }
        #   'flexibility_bands': {
        #       (0,0,0): 0.2, (0,0,1): 100, (0,0,2): 200, (0,1,0): 0.15, ...
        #    }
        #   }
        # }
        result = data
        #print(data)
        self.data = result

    # Method to create model instance with previous passed data directly from the solver class
    def model_create_instance(self):
        return self.model.create_instance(self.data)