import json

from Virtus.solver import Solver

# b = 3
# d = {}
# d[None] = {}
# d[None]['n_timestamps'] = {None: b}
# print(d)

n=24
p=10
m=2

data = {None: {}}
data[None]['n_timestamps'] = {None: n}
data[None]['n_pods'] = {None: p}
data[None]['n_flex_max'] = {None: m}
data[None]['flexibility_bands'] = {}
for i in range(p):
    t1 = (i, 1, 1)
    t2 = (i, 1, 2)
    t3 = (i, 1, 3)
    t4 = (i, 2, 1)
    t5 = (i, 2, 2)
    t6 = (i, 2, 3)
    if i%2==0:
        data[None]['flexibility_bands'][t1] = 0.2
        data[None]['flexibility_bands'][t2] = 100
        data[None]['flexibility_bands'][t3] = 200
        data[None]['flexibility_bands'][t4] = 0.25
        data[None]['flexibility_bands'][t5] = 300
        data[None]['flexibility_bands'][t6] = 450
    else:
        data[None]['flexibility_bands'][t1] = 0.15
        data[None]['flexibility_bands'][t2] = -200
        data[None]['flexibility_bands'][t3] = -100
        data[None]['flexibility_bands'][t4] = 0.1
        data[None]['flexibility_bands'][t5] = -450
        data[None]['flexibility_bands'][t6] = -300

#data = { None: {"n_timestamps": { None: 2 },"n_pods": { None: 2 },"n_flex_param": { None: 3 },"n_flex_max": { None: 2 },"baselines": { (0,0): 200, (0,1): 220, (1,0): 210, (1,1): 180},"flexibility_bands": { (0,1,1): 0.2, (0,1,2): 100, (0,1,3): 200, (0,2,1): 0.25, (0,2,2): 200, (0,2,3): 400,(1,1,1): 0.25, (1,1,2): -150, (1,1,3): -100, (1,2,1): 0.75, (1,2,2): -400, (1,2,3): -300}}}
s = Solver(data)
s.resolve()

print(s.powers_result)
print(s.solution_value)
