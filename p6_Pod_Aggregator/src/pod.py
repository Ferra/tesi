from p6_Pod_Aggregator.src.enums import ModelResolveMethod
from p6_Pod_Aggregator.src.profile import Profile
from p6_Pod_Aggregator.src.solver import Solver
from p6_Pod_Aggregator.src.storage import SimpleStorage
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

sns.set()


class Pod(object):

    def __init__(self, profiles = None):
        if profiles is None:
            self.profiles = []
        else:
            self.set_profiles(profiles)
        self.solver = None
        self.resolve_method = None

    def add_profile(self, profile):
        if isinstance(profile, Profile):
            # Overwrite Storage profile if already exist
            if isinstance(profile, SimpleStorage):
                self.profiles = [p for p in self.profiles if not isinstance(p, SimpleStorage)]
            self.profiles.append(profile)
        else:
            raise Exception('{} passed argument is not of type Profile'.format(profile))

    def set_profiles(self, profiles: [Profile]):
        try:
            for p in profiles:
                self.add_profile(p)
        except:
            raise Exception('Profiles is not iterable')

    def resolve(self, model_resolve_method=ModelResolveMethod.MINIMIZE_AND_MAXIMIZE, print_results=False, print_graphs=False, tee=False, pprint=False):

        self.resolve_method = model_resolve_method
        self.solver = None
        self.solver = Solver(self.profiles)

        self.solver.resolve(model_resolve_method, print_results, tee, pprint)

        if print_graphs:
            if model_resolve_method == ModelResolveMethod.MINIMIZE:
                self.print_graph('minimized')
            elif model_resolve_method == ModelResolveMethod.MAXIMIZE:
                self.print_graph('maximized')
            elif model_resolve_method == ModelResolveMethod.MINIMIZE_AND_MAXIMIZE:
                self.print_graph('minimized')
                self.print_graph('maximized')
                self.print_graph_2()

    def get_costs(self):

        if self.solver is None:
            print('Before getting the costs you have to call \'resolve()\' and resolve the optimization.')
        else:
            if self.resolve_method == ModelResolveMethod.MINIMIZE:
                return self.solver.results[self.resolve_method]['cost']
            elif self.resolve_method == ModelResolveMethod.MAXIMIZE:
                return self.solver.results[self.resolve_method]['cost']
            elif self.resolve_method == ModelResolveMethod.MINIMIZE_AND_MAXIMIZE:
                return [self.solver.results['minimized']['cost'], self.solver.results['maximized']['cost']]

    def print_graph(self, method):
        fig, ax = plt.subplots(figsize=(20, 15))
        ax.set(xlabel='Timestamp (t)', ylabel='Active Power (MW)',
               title='Model Results - {} - {}'.format(method, self.solver.results[method]['solution_value']))
        plt.xticks(range(0, 96, 5))
        # if self.solver.data['n_load_t1'] > 1:
        #     for index, l in enumerate([p for p in self.solver.l1_array]):
        #         ax.plot(l.profile, label='L1-' + str(index))
        # if self.solver.data['n_load_t2'] > 1:
        #     for index, l in enumerate([p for p in self.solver.l2_array]):
        #         ax.plot(l.profile, label='L2-' + str(index))
        # if self.solver.data['n_load_t3'] > 1:
        #     for index, l in enumerate([p for p in self.solver.l3_array]):
        #         ax.plot(l.profile, label='L3-' + str(index))
        # if self.solver.data['n_chp'] > 1:
        #     for index, chp in enumerate([p for p in self.solver.chp_array]):
        #         ax.plot(chp.profile, label='CHP-' + str(index))
        if self.solver.data['n_load_t1'] > 0:
            ax.plot(sum(l.profile for l in [p for p in self.solver.l1_array]), label='L1-Total')
        if self.solver.data['n_load_t2'] > 0:
            ax.plot(sum(l.profile for l in [p for p in self.solver.l2_array]), label='L2-Total')
            ax.plot(sum(l.profile for l in [p for p in self.solver.l2_array]) + np.array(self.solver.results[method]['load_t2_shift']).sum(axis=0), label='L2-Shifted')
        if self.solver.data['n_load_t3'] > 0:
            ax.plot(sum(l.profile for l in [p for p in self.solver.l3_array]), label='L3-Total')
            ax.plot(sum(l.profile for l in [p for p in self.solver.l3_array]) + np.array(self.solver.results[method]['load_t3_shift']).sum(axis=0), label='L3-Shifted')
        if self.solver.data['n_chp'] > 0:
            ax.plot(sum(chp.profile for chp in [p for p in self.solver.chp_array]), label='CHP-Total')
            ax.plot(sum(chp.profile for chp in [p for p in self.solver.chp_array]) + np.array(self.solver.results[method]['chp_shift']).sum(axis=0), label='CHP-Shifted')
        if self.solver.data['n_pv'] > 0:
            ax.plot(sum(l.profile for l in [p for p in self.solver.pv_array]) + np.array(self.solver.results[method]['pv_shift']).sum(axis=0), label='PV-Shifted')
            ax.plot(sum(l.profile for l in [p for p in self.solver.pv_array]), label='PV-Total')
        if self.solver.data['storage'] > 0:
            ax.plot(self.solver.results[method]['storage_charge'], label='Storage')

        ax.plot(self.solver.results[method]['grid'], label='Grid')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.show()

    def print_graph_2(self):

        fig, ax = plt.subplots(figsize=(20, 15))
        ax.set(xlabel='Timestamp (t)', ylabel='Active Power (MW)', title='Model Grid Results')
        plt.xticks(range(0, 96, 5))

        ax.plot(self.solver.results['minimized']['grid'], label='Grid (minimized)')
        ax.plot(self.solver.results['maximized']['grid'], label='Grid (maximized)')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.show()