from p6_Pod_Aggregator.src.enums import ProfileType
from p6_Pod_Aggregator.src.profile import Profile

# Type 1 loads: not controllable
class LoadT1(Profile):

    def __init__(self, l, timestamps=96):
        super().__init__(l, ProfileType.LOAD, 0, 0, timestamps)

# Type 2 loads: compressible
#   At each timestamp the load can be compressed at most by 'allowed_reduction'
class LoadT2(Profile):

    def __init__(self, l, allowed_t: [], allowed_reduction, cost_min=0, cost_max=0, timestamps=96):
        super().__init__(l, ProfileType.LOAD, cost_min, cost_max, timestamps)
        self.allowed_t = [1 if i in allowed_t else 0 for i in range(self.timestamps)]
        self.allowed_reduction = self.setup_array_for_property(allowed_reduction)


# Type 3 loads: shiftable
#   At each timestamp the load can be shifted by a value between 'min_shift' and 'max_shift'.
#   Total shifting must be equal to 'total_shifting'
#   Setting total_shifting = 0 will setup the daily conservation
class LoadT3(Profile):

    def __init__(self, l, allowed_t: [], min_shift, max_shift, total_shift, cost_min=0, cost_max=0, timestamps=96):
        super().__init__(l, ProfileType.LOAD, cost_min, cost_max, timestamps)
        self.allowed_t = [1 if i in allowed_t else 0 for i in range(self.timestamps)]
        self.min_shift = self.setup_array_for_property(min_shift)
        self.max_shift = self.setup_array_for_property(max_shift)
        self.total_shift = total_shift