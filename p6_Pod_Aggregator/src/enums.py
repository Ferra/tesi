from enum import Enum

class ProfileType(Enum):
    LOAD = 1
    PV = 2
    CHP = 3
    STORAGE = 4
    PRICE = 5
    ZERO = 6
    NA = 7          # Not available, not specified

class ModelResolveMethod(Enum):
    MINIMIZE = 1
    MAXIMIZE = 2
    MINIMIZE_AND_MAXIMIZE = 3