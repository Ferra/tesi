from p6_Pod_Aggregator.src.enums import ProfileType
from p6_Pod_Aggregator.src.profile import Profile


class SimpleStorage(Profile):

    def __init__(self, initial_charge, max_charge, max_delta_charge, max_delta_discharge, charge_efficiency, discharge_efficiency, timestamps=96):
        super().__init__([0] * timestamps, ProfileType.STORAGE, 0, 0, timestamps)
        self.initial_charge = initial_charge
        self.max_charge = max_charge
        self.max_delta_charge = self.setup_array_for_property(max_delta_charge)
        self.max_delta_discharge = self.setup_array_for_property(max_delta_discharge)
        self.charge_efficiency = charge_efficiency
        self.discharge_efficiency = discharge_efficiency