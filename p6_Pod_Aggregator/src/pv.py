from p6_Pod_Aggregator.src.enums import ProfileType
from p6_Pod_Aggregator.src.profile import Profile


class PV(Profile):

    def __init__(self, l, min_shift, max_shift, total_shift, cost_min=0, cost_max=0, timestamps=96):
        super().__init__(l, ProfileType.PV, cost_min, cost_max, timestamps)
        # Using profile percentage for min_shift and max_shift allows the shift only if the profile is positive
        self.min_shift = self.setup_array_for_property(l*(min_shift/100))
        self.max_shift = self.setup_array_for_property(l*(max_shift/100))
        self.total_shift = total_shift

