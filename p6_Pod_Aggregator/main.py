import random

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import csv

sns.set()

from p6_Pod_Aggregator.src.aggregator import Aggregator
from p6_Pod_Aggregator.src.enums import ModelResolveMethod
from p6_Pod_Aggregator.src.load import LoadT1, LoadT2, LoadT3
from p6_Pod_Aggregator.src.pod import Pod
from p6_Pod_Aggregator.src.pv import PV
from p6_Pod_Aggregator.src.solver import Solver
from p6_Pod_Aggregator.src.storage import SimpleStorage


np.random.seed(10)

# Import pv profiles in array
pv_profiles = np.load('../Profili/processed_pv.npy')
milling_machines_I = np.load('../Profili/millingmachine-I.npy')
milling_machines_II = np.load('../Profili/millingmachine-II.npy')
doublepolecontactor_I = np.load('../Profili/doublepolecontactor-I.npy')
doublepolecontactor_II = np.load('../Profili/doublepolecontactor-II.npy')
pelletizer_I = np.load('../Profili/pelletizer-I.npy')
pelletizer_II = np.load('../Profili/pelletizer-II.npy')

p1_pv = PV(pv_profiles[11] * 20000,-10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p1_l1 = LoadT1(milling_machines_I[8])
p1_l2 = LoadT2(milling_machines_I[7], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75], 10000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p1_l3 = LoadT3(milling_machines_I[9], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -5000000, 5000000, -1000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p1_st = SimpleStorage(0, 20000000, 1000000, 1000000, 0.95, 0.95)

p2_pv = PV(pv_profiles[10] * 2000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p2_l2 = LoadT2(doublepolecontactor_I[8], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90], 1000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p2_l2_2 = LoadT2(doublepolecontactor_I[7], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90], 1000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p2_l3 = LoadT3(doublepolecontactor_II[9], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -500000, 500000, -100000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))

p3_pv = PV(pv_profiles[12] * 200000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p3_pv_2 = PV(pv_profiles[13] * 200000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p3_l3 = LoadT3(pelletizer_I[9], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -50000000, 50000000, -10000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p3_l3_2 = LoadT3(pelletizer_I[10], [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -50000000, 50000000, -10000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p3_l3_3 = LoadT3(pelletizer_I[11], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -50000000, 50000000, -10000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))

p4_pv = PV(pv_profiles[9] * 20000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p4_pv_2 = PV(pv_profiles[8] * 20000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p4_pv_3 = PV(pv_profiles[7] * 20000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p4_l3 = LoadT3(milling_machines_I[13], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -10000000, 10000000, -100000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p4_st = SimpleStorage(0, 20000000, 1000000, 1000000, 0.95, 0.95)

p5_pv = PV(pv_profiles[16] * 100000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p5_pv_2 = PV(pv_profiles[17] * 100000, -10, 10, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p5_l2 = LoadT2(pelletizer_I[14], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], 20000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p5_l3 = LoadT3(milling_machines_II[7], [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -50000000, 50000000, -100000000, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p5_l3_2 = LoadT3(milling_machines_II[8], [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65], -100000000, 100000000, 0, cost_min=np.random.uniform(-1, 1, size=(96,)), cost_max=np.random.uniform(-1, 1, size=(96,)))
p5_st = SimpleStorage(0, 100000000, 10000000, 10000000, 0.95, 0.95)


# solver_method = ModelResolveMethod.MINIMIZE_AND_MAXIMIZE

aggregator = Aggregator()

p1 = Pod()
p1.add_profile(p1_pv)
p1.add_profile(p1_l1)
p1.add_profile(p1_l2)
p1.add_profile(p1_l3)
p1.add_profile(p1_st)

p2 = Pod()
p2.add_profile(p2_pv)
p2.add_profile(p2_l2)
p2.add_profile(p2_l2_2)
p2.add_profile(p2_l3)

p3 = Pod()
p3.add_profile(p3_pv)
p3.add_profile(p3_pv_2)
p3.add_profile(p3_l3)
p3.add_profile(p3_l3_2)
p3.add_profile(p3_l3_3)

p4 = Pod()
p4.add_profile(p4_pv)
p4.add_profile(p4_pv_2)
p4.add_profile(p4_pv_3)
p4.add_profile(p4_l3)
p4.add_profile(p4_st)

p5 = Pod()
p5.add_profile(p5_pv)
p5.add_profile(p5_pv_2)
p5.add_profile(p5_l2)
p5.add_profile(p5_l3)
p5.add_profile(p5_l3_2)
p5.add_profile(p5_st)


p1.resolve(print_results=True, print_graphs=True, tee=False, pprint=False)
p2.resolve(print_results=True, print_graphs=False, tee=False, pprint=False)
p3.resolve(print_results=True, print_graphs=False, tee=False, pprint=False)
p4.resolve(print_results=True, print_graphs=False, tee=False, pprint=False)
p5.resolve(print_results=True, print_graphs=False, tee=False, pprint=False)

# print(p1.get_costs())

# s = Solver()
# s.add_profile(pv)
# s.add_profile(l1)
# s.add_profile(l2)
# s.add_profile(l3)
# s.add_profile(st)

# s.resolve(solver_method, print_results=True, print_graphs=False, tee=False, pprint=False)

aggregator.add_pod(p1)
aggregator.add_pod(p2)
aggregator.add_pod(p3)
aggregator.add_pod(p4)
aggregator.add_pod(p5)
result = aggregator.aggregate()

fig, ax = plt.subplots(figsize=(20, 15))
ax.set(xlabel='Flexibility', ylabel='Active Power (MW)', title='Aggregated Grid Results')
plt.xticks(range(0, 96, 5))

ax.plot(result['minimized']['flexibility'], label='Grid (minimized)')
ax.plot(result['maximized']['flexibility'], label='Grid (maximized)')
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

fig, ax = plt.subplots(figsize=(20, 15))
ax.set(xlabel='Cost', ylabel='Cost', title='Aggregated Cost Results')
plt.xticks(range(0, 96, 5))

ax.plot(result['minimized']['cost'], label='Cost (minimized)')
ax.plot(result['maximized']['cost'], label='Cost (maximized)')
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()


for index, pod in enumerate(aggregator.pods, start=1):
    pv_index = 0
    l1_index = 0
    l2_index = 0
    l3_index = 0
    with open('pod_'+str(index)+'.csv', mode='w') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')

        names = ['type']
        names = np.append(names, [str(i) for i in range(0, 96)])
        writer.writerow(names)

        for profile in pod.profiles:

            if isinstance(profile, PV):
                prof_type = 'PV-' + str(pv_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'PV-' + str(pv_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['pv_shift'][pv_index])
                writer.writerow(new_line)
                prof_type = 'PV-' + str(pv_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['pv_shift'][pv_index])
                writer.writerow(new_line)
                pv_index += 1
            if isinstance(profile, LoadT1):
                prof_type = 'L1-' + str(l1_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L1-' + str(l1_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L1-' + str(l1_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                l1_index += 1
            if isinstance(profile, LoadT2):
                prof_type = 'L2-' + str(l2_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L2-' + str(l2_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['load_t2_shift'][l2_index])
                writer.writerow(new_line)
                prof_type = 'L2-' + str(l2_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['load_t2_shift'][l2_index])
                writer.writerow(new_line)
                l2_index += 1
            if isinstance(profile, LoadT3):
                prof_type = 'L3-' + str(l3_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L3-' + str(l3_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['load_t3_shift'][l3_index])
                writer.writerow(new_line)
                prof_type = 'L3-' + str(l3_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['load_t3_shift'][l3_index])
                writer.writerow(new_line)
                l3_index += 1
            if isinstance(profile, SimpleStorage):
                prof_type = 'ST-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'ST-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, pod.solver.results['minimized']['storage_charge'])
                writer.writerow(new_line)
                prof_type = 'ST-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['storage_charge'])
                writer.writerow(new_line)

        new_line = ['Grid-output-min']
        new_line = np.append(new_line, pod.solver.results['minimized']['grid'])
        writer.writerow(new_line)
        new_line = ['Grid-output-max']
        new_line = np.append(new_line, pod.solver.results['maximized']['grid'])
        writer.writerow(new_line)
        new_line = ['Cost-output-min']
        new_line = np.append(new_line, pod.solver.results['minimized']['cost'])
        writer.writerow(new_line)
        new_line = ['Cost-output-max']
        new_line = np.append(new_line, pod.solver.results['maximized']['cost'])
        writer.writerow(new_line)