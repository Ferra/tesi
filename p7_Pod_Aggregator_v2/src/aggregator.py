from p7_Pod_Aggregator_v2.src.pod import Pod


class Aggregator(object):

    def __init__(self, pods=None):
        if pods is None:
            self.pods = []
        else:
            self.set_pods(pods)
        # self.solver = None
        self.resolve_method = None
        self.result = {
            'minimized': {
                'flexibility': [0] * 96,  # TODO: change 96 to timestamps
                'cost': [0] * 96
            },
            'maximized': {
                'flexibility': [0] * 96,
                'cost': [0] * 96
            },
        }

    def add_pod(self, pod):
        if isinstance(pod, Pod):
            self.pods.append(pod)
        else:
            raise Exception('{} passed argument is not of type Pod'.format(pod))

    def set_pods(self, pods: [Pod]):
        try:
            for p in pods:
                self.add_pod(p)
        except:
            raise Exception('pods is not iterable')

    def resolve_pods_and_aggregate(self):
        for pod in self.pods:
            pod.resolve()
        return self.aggregate()


        # self.result['minimized']['flexibility'] = [sum(pod.solver.results['minimized']['grid']) for pod in self.pods]
        # self.result['maximized']['flexibility'] = [sum(pod.solver.results['maximized']['grid']) for pod in self.pods]
        # self.result['minimized']['cost'] = [sum(pod.solver.results['minimized']['cost']) for pod in self.pods]
        # self.result['maximized']['cost'] = [sum(pod.solver.results['minimized']['cost']) for pod in self.pods]

    def aggregate(self):
        for pod in self.pods:
            self.result['minimized']['flexibility'] = [sum(x) for x in zip(self.result['minimized']['flexibility'], pod.solver.results['minimized']['grid'])]
            self.result['maximized']['flexibility'] = [sum(x) for x in zip(self.result['maximized']['flexibility'], pod.solver.results['maximized']['grid'])]
            self.result['minimized']['cost'] = [sum(x) for x in zip(self.result['minimized']['cost'], pod.solver.results['minimized']['cost'])]
            self.result['maximized']['cost'] = [sum(x) for x in zip(self.result['maximized']['cost'], pod.solver.results['maximized']['cost'])]


        # for i in range(0, 96):
        #     if self.result['minimized']['flexibility'][i] > self.result['maximized']['flexibility'][i]:
        #         x = self.result['minimized']['flexibility'][i]
        #         self.result['minimized']['flexibility'][i] = self.result['maximized']['flexibility'][i]
        #         self.result['maximized']['flexibility'][i] = x

        return self.result
