from p7_Pod_Aggregator_v2.src.enums import ProfileType
from p7_Pod_Aggregator_v2.src.profile import Profile


class Wind(Profile):

    def __init__(self, l, size, min_shift=8, max_shift=8, total_shift=0, cost_min=0, cost_max=0, timestamps=96):
        super().__init__([x*size for x in l], ProfileType.WIND, cost_min, cost_max, timestamps)
        # Using profile percentage for min_shift and max_shift allows the shift only if the profile is positive
        self.size = size
        self.min_shift = self.setup_array_for_property(-l*(min_shift/100)*self.size)
        self.max_shift = self.setup_array_for_property(l*(max_shift/100)*self.size)
        self.total_shift = total_shift
