from enum import Enum

class ProfileType(Enum):
    LOAD = 1
    PV = 2
    WIND = 3
    CHP = 4
    STORAGE = 5
    PRICE = 6
    ZERO = 7
    NA = 8          # Not available, not specified

class ModelResolveMethod(Enum):
    MINIMIZE = 1
    MAXIMIZE = 2
    MINIMIZE_AND_MAXIMIZE = 3