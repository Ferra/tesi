import random

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import csv

sns.set()

from p7_Pod_Aggregator_v2.src.aggregator import Aggregator
from p7_Pod_Aggregator_v2.src.enums import ModelResolveMethod
from p7_Pod_Aggregator_v2.src.load import LoadT1, LoadT2, LoadT3
from p7_Pod_Aggregator_v2.src.pod import Pod
from p7_Pod_Aggregator_v2.src.pv import PV
from p7_Pod_Aggregator_v2.src.wind import Wind
from p7_Pod_Aggregator_v2.src.solver import Solver
from p7_Pod_Aggregator_v2.src.storage import SimpleStorage


np.random.seed(10)

# Import pv profiles in array
pv_profiles = np.load('../Profili/processed_pv.npy')
pv_unige = np.load('../Profili/pv_unige_darsena1.npy')
wind_profiles = np.load('../Profili/wind_BLUFF1.npy')
milling_machines_I = np.load('../Profili/millingmachine-I.npy')
milling_machines_II = np.load('../Profili/millingmachine-II.npy')
doublepolecontactor_I = np.load('../Profili/doublepolecontactor-I.npy')
doublepolecontactor_II = np.load('../Profili/doublepolecontactor-II.npy')
pelletizer_I = np.load('../Profili/pelletizer-I.npy')
pelletizer_II = np.load('../Profili/pelletizer-II.npy')

scale_factor = 1000#1000000

p1_pv = PV(pv_unige[10], 100*scale_factor)
p1_pv_2 = PV(pv_unige[11], 150*scale_factor)
p1_st = SimpleStorage(70*scale_factor)
p1_l2 = LoadT2([x/1 for x in doublepolecontactor_I[8]], list(range(0, 96)), 25)
p1_l3 = LoadT3([x/10000 for x in pelletizer_I[8]], list(range(0, 96)), 0)

p2_pv = PV(pv_unige[8], 50*scale_factor)
p2_wind = Wind(wind_profiles[8], 200*scale_factor)
p2_l2 = LoadT2(doublepolecontactor_II[8], list(range(0, 96)), 30)
s = list(range(68, 88))
p2_l3 = LoadT3([x/10000 for x in pelletizer_II[7]], [x for x in list(range(0, 96)) if x not in s], 0)

p3_pv = PV(pv_unige[12], 20*scale_factor)
p3_pv_2 = PV(pv_unige[13], 100*scale_factor)
p3_st = SimpleStorage(30*scale_factor)
p3_l2 = LoadT2([x/10 for x in doublepolecontactor_I[11]], list(range(0, 96)), 10)
p3_l3 = LoadT3([x/100 for x in pelletizer_II[11]], list(range(0, 96)), 15)

# solver_method = ModelResolveMethod.MINIMIZE_AND_MAXIMIZE

aggregator = Aggregator()

p1 = Pod()
p1.add_profile(p1_pv)
p1.add_profile(p1_pv_2)
p1.add_profile(p1_st)
p1.add_profile(p1_l2)
p1.add_profile(p1_l3)

p2 = Pod()
p2.add_profile(p2_pv)
p2.add_profile(p2_wind)
p2.add_profile(p2_l2)
p2.add_profile(p2_l3)

p3 = Pod()
p3.add_profile(p3_pv)
p3.add_profile(p3_pv_2)
p3.add_profile(p3_st)
p3.add_profile(p3_l2)
p3.add_profile(p3_l3)

p1.resolve(print_results=True, print_graphs=False, tee=False, pprint=False, per_allegra=False)
p2.resolve(print_results=True, print_graphs=True, tee=False, pprint=False, per_allegra=False)
p3.resolve(print_results=True, print_graphs=False, tee=False, pprint=False, per_allegra=False)

aggregator.add_pod(p1)
aggregator.add_pod(p2)
aggregator.add_pod(p3)
result = aggregator.aggregate()

print_results = True

if print_results:

    fig, ax = plt.subplots(figsize=(20, 15))
    ax.set(xlabel='Flexibility', ylabel='Active Power (W)', title='Aggregated Grid Results')
    plt.xticks(range(0, 96, 5))

    ax.plot(result['minimized']['flexibility'], label='Grid (minimized)')
    ax.plot(result['maximized']['flexibility'], label='Grid (maximized)')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()

    fig, ax = plt.subplots(figsize=(20, 15))
    ax.set(xlabel='Cost', ylabel='Cost', title='Aggregated Cost Results')
    plt.xticks(range(0, 96, 5))

    ax.plot(result['minimized']['cost'], label='Cost (minimized)')
    ax.plot(result['maximized']['cost'], label='Cost (maximized)')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.show()

'''

# SAVE RESULTS IN CSV FILE

for index, pod in enumerate(aggregator.pods, start=1):
    pv_index = 0
    wind_index = 0
    l1_index = 0
    l2_index = 0
    l3_index = 0
    with open('pod_'+str(index)+'.csv', mode='w') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')

        names = ['type']
        names = np.append(names, [str(i) for i in range(0, 96)])
        writer.writerow(names)

        for profile in pod.profiles:

            if isinstance(profile, PV):
                prof_type = 'PV-' + str(pv_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'PV-' + str(pv_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['pv_shift'][pv_index])
                writer.writerow(new_line)
                prof_type = 'PV-' + str(pv_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['pv_shift'][pv_index])
                writer.writerow(new_line)
                pv_index += 1
            if isinstance(profile, Wind):
                prof_type = 'Wind-' + str(wind_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'Wind-' + str(wind_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['wind_shift'][wind_index])
                writer.writerow(new_line)
                prof_type = 'Wind-' + str(pv_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['wind_shift'][wind_index])
                writer.writerow(new_line)
                wind_index += 1
            if isinstance(profile, LoadT1):
                prof_type = 'L1-' + str(l1_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L1-' + str(l1_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L1-' + str(l1_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                l1_index += 1
            if isinstance(profile, LoadT2):
                prof_type = 'L2-' + str(l2_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L2-' + str(l2_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['load_t2_shift'][l2_index])
                writer.writerow(new_line)
                prof_type = 'L2-' + str(l2_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['load_t2_shift'][l2_index])
                writer.writerow(new_line)
                l2_index += 1
            if isinstance(profile, LoadT3):
                prof_type = 'L3-' + str(l3_index) + '-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'L3-' + str(l3_index) + '-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['minimized']['load_t3_shift'][l3_index])
                writer.writerow(new_line)
                prof_type = 'L3-' + str(l3_index) + '-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['load_t3_shift'][l3_index])
                writer.writerow(new_line)
                l3_index += 1
            if isinstance(profile, SimpleStorage):
                prof_type = 'ST-input'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile.copy())
                writer.writerow(new_line)
                prof_type = 'ST-output-min'
                new_line = [prof_type]
                new_line = np.append(new_line, pod.solver.results['minimized']['storage_charge'])
                writer.writerow(new_line)
                prof_type = 'ST-output-max'
                new_line = [prof_type]
                new_line = np.append(new_line, profile.profile + pod.solver.results['maximized']['storage_charge'])
                writer.writerow(new_line)

        new_line = ['Grid-output-min']
        new_line = np.append(new_line, pod.solver.results['minimized']['grid'])
        writer.writerow(new_line)
        new_line = ['Grid-output-max']
        new_line = np.append(new_line, pod.solver.results['maximized']['grid'])
        writer.writerow(new_line)
        new_line = ['Cost-output-min']
        new_line = np.append(new_line, pod.solver.results['minimized']['cost'])
        writer.writerow(new_line)
        new_line = ['Cost-output-max']
        new_line = np.append(new_line, pod.solver.results['maximized']['cost'])
        writer.writerow(new_line)

'''