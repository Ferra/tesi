import pyomo.environ as pyo

class FirstModel:

    model = pyo.AbstractModel()

    def __init__(self):
        self.__setup_model()
        self.__setup_vars()
        self.__setup_parameters()
        self.__setup_constraints()
        self.__setup_objective()

    def __setup_model(self):

        # Number of timestamps
        self.model.n_timestamps = pyo.Param(domain=pyo.PositiveIntegers)

        # Number of load profiles
        self.model.n_loads = pyo.Param(domain=pyo.PositiveIntegers)

        self.model.T = pyo.RangeSet(0, self.model.n_timestamps - 1)
        self.model.N = pyo.RangeSet(0, self.model.n_loads - 1)
        self.model.F = pyo.RangeSet(0, 80)


    def __setup_vars(self):

        # Result: new profile of each load
        self.model.load_result = pyo.Var(self.model.N, self.model.T, domain=pyo.Reals)  # TODO: change real type? (Should be NonNegativeReals, but we also have negative load)

        # Difference between original profile and new profile (to have the integral equal to zero)
        self.model.load_delta = pyo.Var(self.model.N, self.model.T, domain=pyo.Reals)

        # Aggregated load
        self.model.load_aggregated = pyo.Var(self.model.T, domain=pyo.Reals)

        # Aggregated new load
        self.model.load_result_aggregated = pyo.Var(self.model.T, domain=pyo.Reals)

        # Energy acquired from the grid
        self.model.grid_in = pyo.Var(self.model.T, domain=pyo.PositiveReals)

        # Energy sold to the grid
        self.model.grid_out = pyo.Var(self.model.T, domain=pyo.PositiveReals)

        # HELPER VARS
        self.model.load_full = pyo.Var(self.model.N, self.model.T, domain=pyo.Binary)
        self.model.load_min = pyo.Var(self.model.N, self.model.T, domain=pyo.Binary)

    def __setup_parameters(self):

        # Load profiles (n profiles, with t time-step each)
        self.model.load_profiles = pyo.Param(self.model.N, self.model.T, domain=pyo.Reals)

        # PV profile (t time-step each)
        self.model.pv_profile = pyo.Param(self.model.T, domain=pyo.Reals)

        # Output when in minimal effort mode
        self.model.load_minimal = pyo.Param(self.model.N, self.model.T, domain=pyo.Reals)

        # Corrected baseline
        self.model.corrected_baseline = pyo.Param(self.model.T, domain=pyo.Reals)


    def __setup_constraints(self):

        def integral_equal_to_zero(m, n):
            return sum(m.load_delta[n, t] for t in m.F) == 0
        #self.model.integral_equal_to_zero = pyo.Constraint(self.model.N, rule=integral_equal_to_zero)

        def new_load_definition(m, n, t):
            #return  m.load_profiles[n, t] == m.load_result[n, t] or m.load_result[n, t] == 0 or m.load_result[n, t] == m.load_minimal[n, t]
            return m.load_result[n, t] == m.load_profiles[n, t]*m.load_full[n, t] + m.load_minimal[n, t]*m.load_min[n, t]
        self.model.new_load_definition = pyo.Constraint(self.model.N, self.model.T, rule=new_load_definition)

        def full_min(m, n, t):
            return m.load_full[n, t] + m.load_min[n, t] <= 1
        self.model.full_min = pyo.Constraint(self.model.N, self.model.T, rule=full_min)

        def delta_load_definition(m, n, t):
            return m.load_delta[n, t] == m.load_profiles[n, t] - m.load_result[n, t]
        self.model.delta_load_definition = pyo.Constraint(self.model.N, self.model.T, rule=delta_load_definition)

        def aggregated_load_definition(m, t):
            return m.load_aggregated[t] == sum(m.load_profiles[n, t] for n in m.N)
        self.model.aggregated_load_definition = pyo.Constraint(self.model.T, rule=aggregated_load_definition)

        def aggregated_load_result_definition(m, t):
            return m.load_result_aggregated[t] == sum(m.load_result[n, t] for n in m.N)
        self.model.aggregated_load_result_definition = pyo.Constraint(self.model.T, rule=aggregated_load_result_definition)

        def power_balance(m, t):
            return m.load_result_aggregated[t] - m.pv_profile[t] + m.grid_out[t] - m.grid_in[t] == 0
        self.model.power_balance = pyo.Constraint(self.model.T, rule=power_balance)

    def __setup_objective(self):

        def objective_function(m):
            return sum((m.corrected_baseline[t] - m.load_result_aggregated[t])**2 for t in m.T)
        self.model.objective_function = pyo.Objective(sense=pyo.minimize, rule=objective_function)


    def create_instance(self, data):
        return self.model.create_instance(data)