from Aggregator.src.model import FirstModel
import pyomo.environ as pyo
from pyomo.opt import SolverStatus, TerminationCondition
from pyomo.core import Var, Objective

class Solver:

    def __init__(self, data):
        self.data = self.__init_data(data)
        self.n_loads = self.data[None]['n_loads'][None]
        self.n_timestamps = self.data[None]['n_timestamps'][None]

        self.load_result = [[0 for _ in range(self.n_timestamps)] for _ in range(self.n_loads)]
        self.load_full_bool = [[0 for _ in range(self.n_timestamps)] for _ in range(self.n_loads)]
        self.load_min_bool = [[0 for _ in range(self.n_timestamps)] for _ in range(self.n_loads)]
        self.load_aggregated = [0 for _ in range(self.n_timestamps)]
        self.load_result_aggregated = [0 for _ in range(self.n_timestamps)]
        self.grid_in = [0 for _ in range(self.n_timestamps)]
        self.grid_out = [0 for _ in range(self.n_timestamps)]
        self.solution_value = None # For the solution value
        self.model = FirstModel()

    def __init_data(self, data):
        result = {None: {}}
        result[None]['n_timestamps'] = {None: data['n_timestamps']}
        result[None]['n_loads'] = {None: data['n_loads']}
        return result

    def add_array_data_field(self, field_name, data):
        self.data[None][field_name] = {}
        for i in range(len(data)):
            self.data[None][field_name][i] = data[i]

    def add_multidimension_array_data_field(self, field_name, data, nested_dimension):
        self.data[None][field_name] = {}
        for i in range(len(data)):
            for j in range(nested_dimension):
                self.data[None][field_name][(i, j)] = data[i][j]

    def resolve(self, tee=True, pprint=False):
        instance = self.model.create_instance(self.data)
        solver = pyo.SolverFactory('gurobi', solver_io="python")
        #solver = pyo.SolverFactory('cplex', executable='/Applications/CPLEX_Studio129/cplex/bin/x86-64_osx/cplex')
        #solver = pyo.SolverFactory('ipopt')
        #solver.options['TimeLimit'] = 5
        result = solver.solve(instance, tee=tee)

        if pprint:
            instance.pprint()

        # Check the results and extract the values if the solution is optimal
        if (result.solver.status == SolverStatus.ok) and (
                result.solver.termination_condition == TerminationCondition.optimal):

            # instance.pprint()

            for v in instance.component_objects(Var, active=True):
                # print ("Variable",v)
                varobject = getattr(instance, str(v))
                if v.name == 'load_result':
                    for index in varobject:
                        self.load_result[index[0]][index[1]] = varobject[index].value
                if v.name == 'load_aggregated':
                    for index in varobject:
                        self.load_aggregated[index] = varobject[index].value
                if v.name == 'load_result_aggregated':
                    for index in varobject:
                        self.load_result_aggregated[index] = varobject[index].value
                if v.name == 'grid_in':
                    for index in varobject:
                        self.grid_in[index] = varobject[index].value
                if v.name == 'grid_out':
                    for index in varobject:
                        self.grid_out[index] = varobject[index].value
                if v.name == 'load_full':
                    for index in varobject:
                        self.load_full_bool[index[0]][index[1]] = varobject[index].value
                if v.name == 'load_min':
                    for index in varobject:
                        self.load_min_bool[index[0]][index[1]] = varobject[index].value
            for v in instance.component_objects(Objective, active=True):
                self.solution_value = v.expr()
            #print("%.2f" % self.solution_value)

            #return self

        elif (result.solver.termination_condition == TerminationCondition.infeasible):
            print('############################ INFEASIBLE MODEL ############################')
            raise Exception('############################ INFEASIBLE MODEL ############################')
        else:
            print('############################ SOMETHING WENT WRONG ############################')
            print("Solver Status: ", result.solver.status)
            raise Exception(
                "############################ SOMETHING WENT WRONG ############################\nSolver Status: ",
                result.solver.status)


    def load_result(self):
        return self.load_result

    def load_aggregated(self):
        return self.load_aggregated

    def load_result_aggregated(self):
        return self.load_result_aggregated

    def grid_in(self):
        return self.grid_in

    def grid_out(self):
        return self.grid_out

    def load_full_bool(self):
        return self.load_full_bool

    def load_min_bool(self):
        return self.load_min_bool

    def solution_value(self):
        return self.solution_value