import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from Aggregator.src.profile import Profile
from Aggregator.src.profile_type import ProfileType
from Aggregator.src.solver import Solver

sns.set()

pv_profiles = np.load('../Profili/processed_pv.npy')
load_profiles = np.load('../Profili/processed_aggregated_load.npy')
prices_profiles = np.load('../Profili/processed_prices.npy')

p = Profile(prices_profiles[11], ProfileType.PRICE)
pv = Profile(pv_profiles[11]*100000, ProfileType.PV)

millingmachine_Is = np.load('../Profili/millingmachine-I.npy')
millingmachine_IIs = np.load('../Profili/millingmachine-II.npy')

millingmachine_I = Profile(millingmachine_Is[11], ProfileType.LOAD)
millingmachine_II = Profile(millingmachine_IIs[11], ProfileType.LOAD)

load_minimal = [15000000]*millingmachine_I.timestamps

def create_baseline(load, threshold, repeat, percentage = 0):
    result = load
    for _ in range(repeat):
        for i in range(1, len(load)-1):
            diff = min(abs(result[i] - result[i - 1]), abs(result[i + 1] - result[i]))
            if diff > threshold:
                result[i] = (result[i-1]+result[i+1])/2
            #if diff == 0:
            #    result[i] = (result[1]+result[len(result)-1])/2
    result = [result[i] - percentage/100*result[i] for i in range(len(result))]
    return result

#baseline = [45000000]*millingmachine_I.timestamps
baseline = create_baseline(millingmachine_I.profile+millingmachine_II.profile, threshold=1, repeat=10, percentage=0)
data = {'n_timestamps': millingmachine_I.timestamps, 'n_loads': 2}

loads = [millingmachine_I.profile, millingmachine_II.profile]
minimal = [load_minimal, load_minimal]

s = Solver(data)
s.add_array_data_field('corrected_baseline', baseline)
s.add_array_data_field('pv_profile', pv.profile)
s.add_multidimension_array_data_field('load_profiles', loads, millingmachine_I.timestamps)
s.add_multidimension_array_data_field('load_minimal', minimal, millingmachine_I.timestamps)
s.resolve(tee=True, pprint=True)

print('Optimal solution: %5.2f' %s.solution_value)

print(s.load_full_bool)

#plt.plot(millingmachine_I.profile, label='1')
#plt.plot(millingmachine_II.profile, label='2')
# plt.plot(s.load_result[0], label='LR1')
# plt.plot(millingmachine_I.profile, label='L1')
# plt.plot(s.load_result[1], label='LR2')
# plt.plot(millingmachine_II.profile, label='L2')
# plt.plot(s.load_aggregated, label='Load')
plt.plot(baseline, label='Baseline')
# plt.plot(s.load_result_aggregated, label='Result')
plt.plot(pv.profile, label='pv')
plt.plot(s.load_result_aggregated, label='Result')
plt.plot(s.grid_in, label='grid in')
plt.plot(s.grid_out, label='grid out')
plt.legend()
plt.show()
