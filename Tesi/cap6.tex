\chapter{Practical use case}

The model described in the chapter \ref{chap:5} is a general, modular and scalable approximation of a virtual power plant. More specific DERs or loads can be easily implemented and integrated into the model.

This model can also be integrated in existing projects that require to simulate the functionality of a virtual power plant and can provide realistic and fast results.

The model performances are tested inside the \textit{VIRTUS} project, which offers a real full architecture in which the model of the VPP can be implemented.

\section{VIRTUS project}

The VIRTUS project (\textit{gestione VIRTUale di riSorse energetiche distribuite}) \cite{virtus} implements a real virtual power plant prototype in an industry context.

With the use of high-tech ICT systems, this project aims to provide to the energy providers the possibility of managing the renewable energy sources aggregating the flexibility of each single DER. 

Integrating the proposed model in the VIRTUS architecture means providing a day-ahead prediction on the flexibility that a real virtual power plant system can provide to the TSO like Terna.

The model is tested with some external datasets, but a MQTT module is added to the project in order to get VIRTUS real data from the online platform LIBRA.

\section{Dataset}\label{sec:dataset}

The dataset of PV profiles are real data provided from real production sites inside the VIRTUS project. The profiles are normalized and are sampled each fifteen-minutes. These profiles will be upload on the LIBRA platform from which they can be taken via MQTT. For this reason, a MQTT module based on the library \textit{paho-mqtt}\footnote{\url{https://pypi.org/project/paho-mqtt/}} has been implemented for a future use. 

For the wind profiles, the public dataset \cite{wind-dataset} is used. It presents five minute resolution wind power data from 22 wind farms in south-east Australia from January 2012 to December 2013. By default, the proposed model uses fifteen-minutes intervals and that's why a first pre-processing is needed.

The load profiles are taken from \cite{cg5v-dk02-18}. This dataset contains heavy-machinery data from the Brazilian industrial sector. Samples are collected from December 2017 until April 2018, which roughly corresponds to 111 days. The data come from different types of machines (millingmachine-I and II, doublepolecontactor-I and II, pelletizer-I and II and exhaustfan-I and II), which will be considered as different type of load in the proposed model. The sampling resolution of the dataset is one second, and that's why a pre-processing aggregation is needed.

The parameters of the storage system are taken from \cite{conte2020optimization}.

Considering the lack of real data for the costs related to the flexibility, the task of the aggregator of performing the aggregation of the costs is not described in these results, because the leads to misunderstands and to results that will not respect a real case.

\section{Perfomance analysis}

The model is tested with different POD configurations to test the performance of the optimization problem. As has been said, the number of PODs can reach up to few thousand and the number of profiles in a single POD up to a few dozen.

All the optimizations are done with the Gurobi solver. It has been tested that the differences between Gurobi and a solver like CPLEX are negligible.

Let's first test the performance of the optimizations problems on PODs with a different number of profiles. For each POD, the two optimizations are done sequentially, activating and deactivating the objective functions as described before. Results are shown in table \ref{tab:performances}. As can be seen, this model scales quite well with a growing number of profiles. With an high number of profiles involved in the problem, the times to get the response are reasonably low, reaching up to a second considering the total time of the two sequential optimizations. A further optimization can be represented by doing the two optimizations in parallel rather than in parallel.

\begin{table}[!htb]
\centering
%\resizebox{\linewidth}{!}{%
\begin{tabular}{|c|c|c|c|} 
\hline
Configuration                  & \begin{tabular}[c]{@{}c@{}}Total profiles\\in the POD\end{tabular} & \begin{tabular}[c]{@{}c@{}}Time for \\minimization\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}Time for \\maximization\\(s)\end{tabular}  \\ 
\hline
1 PV, 1 L2                     & 2                                                                  & 0.10                                                                 & 0.05                                                                  \\ 
\hline
1 PV, 1 L2, 1 St               & 3                                                                  & 0.15                                                                 & 0.07                                                                  \\ 
\hline
2 PV, 1 L2, 1 L3, 1 St         & 5                                                                  & 0.17                                                                 & 0.14                                                                  \\ 
\hline
2 PV, 1 wind, 2 L2, 1 L3, 1 St & 7                                                                  & 0.30                                                                 & 0.13                                                                  \\ 
\hline
2 PV, 1 wind, 3 L2, 3 L3, 1 St & 10                                                                 & 0.27                                                                 & 0.21                                                                  \\ 
\hline
2 PV, 3 wind, 3 L2, 5 L3, 1 St & 14                                                                 & 0.33                                                                 & 0.57                                                                  \\ 
\hline
3 PV, 3 wind, 5 L2, 5 L3, 1 St & 17                                                                 & 0.31                                                                 & 0.30                                                                  \\ 
\hline
5 PV, 5 wind, 5 L2, 5 L3, 1 St & 21                                                                 & 0.42                                                                 & 0.37                                                                  \\
\hline
\end{tabular}
%}
\caption{Performances of the model resolving different PODs with different number of profiles.}
\label{tab:performances}
\end{table}

The model allows the PODs to be optimizing by themselves. This means that the optimization of different PODs can be done sequentially or in parallel. For simplicity the method \texttt{resolve\_pods\_and\_aggregate} of the \texttt{Aggregator} class uses a sequential approach but it can be easily converted to a parallel one.

This means that the proposed model leaves to the developer the choice of how to perform the optimization of different PODs. If the number of PODs is high and there are time constraints, a parallel approach should be preferred to a sequential one.

\section{Demand of flexibility}

Let's now talk about the results of the optimization of each POD. As has been said in chapter \ref{chap:5}, the optimization of a single POD aims to find the minimum and the maximum use of the grid, that represent the flexibility of the POD. Then, the aggregator will aggregate all those flexibilities and will provide the desired one to the TSO.

Three different PODs are tested and then aggregated. Here are their specifications and the results obtained.

\subsection{First configuration}

The first POD tested is composed by:

\begin{itemize}
    \item One photovoltaic profile with a size of 100 KW.
    \item One photovoltaic profile with a size of 150 KW.
    \item One storage with a max charge of 70 KW.
    \item One sheddable load (type 2, doublepolecontactor-I) with an allowed total reduction of 25\%.
    \item One shiftable load (type 3, pelletizer-I) that can always be shifted but with no losses.
\end{itemize}

The results can be seen in the figures \ref{fig:c1-min}, \ref{fig:c1-max} and \ref{fig:c1-flex}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c1-min.png}
         \caption{Case study 1 - Minimization problem. \label{fig:c1-min}}
\end{figure}

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c1-max.png}
         \caption{Case study 1 - Maximization problem. \label{fig:c1-max}}
\end{figure}

The data that has been used comes from the dataset described in \ref{sec:dataset}. As can be seen, some load profiles of the dataset can reach a demand much higher than the renewable production of the POD. This is not a problem because allows the POD to manage in a strategic way these demands in order to minimize (or maximize) the grid contribution.

Let's compare the two optimization results:

\begin{enumerate}
    \item The use of the photovoltaic is very different between the minimization and the maximization problem. In the first it has been strongly shifted towards the first part of the day, while in the latter the quantity of the shift is much less. This confirms that the process of shifting can be very useful in a VPP optimization problem and add a great degree of freedom to the model.
    \item Let's then compare the use of the storage: when the problem is minimize the use of the grid the use of this internal resource is frequent while in the maximization problem the model prefers doing (almost) full charge/discharge cycles. Another interesting thing is that in the minimization, in the last part of the day when the loads are switched off and there is not demand, the storage remains discharged while in the other problem energy from the grid is buyed in order to charge it.
    \item The loads. In this POD the load of type 3 doesn't provide a high demand, but the load of type 2 does. It reaches a demand up to 1.700 KW in some moments of the day and the minimization problem relies to the capacity of the load of loosing the 25\% of his daily production to reduce the energy bought from the grid. For obvious reasons the maximization problem decides to not reduce the load, in order to obtain the maximium use of the grid.
\end{enumerate}

The two flexibilities that comes from the first POD are shown in figure \ref{fig:c1-flex}. As has been said, in this POD the main contribution on the minimization of the use of the grid is given by the reduction of the load demand from the timestamp 25 to 40, given a high flexibility contribution in this part of the day.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c1-flex.png}
         \caption{Case study 1 - Flexibilities. \label{fig:c1-flex}}
\end{figure}

\subsection{Second configuration}

The second POD tested is composed by:

\begin{itemize}
    \item One photovoltaic profile with a size of 50 KW.
    \item One wind profile with a size of 200 KW.
    \item One sheddable load (type 2, doublepolecontactor-II) with an allowed total reduction of 30\%.
    \item One shiftable load (type 3, pelletizer-I) that can always be shifted except for times between 17:00 and 22:00 and with no losses.
\end{itemize}

The results can be seen in the figures \ref{fig:c2-min}, \ref{fig:c2-max} and \ref{fig:c2-flex}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c2-min.png}
         \caption{Case study 2 - Minimization problem. \label{fig:c2-min}}
\end{figure}

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c2-max.png}
         \caption{Case study 2 - Maximization problem. \label{fig:c2-max}}
\end{figure}

This POD owns an important wind component but a relative low size photovoltaic one. Also in this case, the load of type 2 represent the main demand that the VPP has to compensate.

\begin{enumerate}
    \item The first interesting and maybe unexpected thing is that the shifted wind profile does not change between the two optimizations. This can probably due to the relative low amount of allowed shift that not affect to much the model. Also, having a relative low contribution from the PV leaves to the wind production the main option to balance the load demand.
    \item As in the first POD, the main requested load is a sheddable one that can be reduced up to the 30\% of his daily production. The minimization problem decides to switch off the the load demand presumably when the there are load peaks and the production of wind and photovoltaic is high (timestamps from 38 to 47 and when the internal productions cannot compensate the requested demand in the last part of the day.
\end{enumerate}

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c2-flex.png}
         \caption{Case study 2 - Flexibilities. \label{fig:c2-flex}}
\end{figure}

In this case, looking at the figure \ref{fig:c2-flex} is evident when the POD can grants flexibility and this correspond to the moments of the day when it decided to reduce to zero the demand of the load of type 2.

\subsection{Third configuration}

The third POD tested is composed by:

\begin{itemize}
    \item One photovoltaic profile with a size of 20 KW.
    \item One photovoltaic profile with a size of 100 KW.
    \item One storage with a max charge of 30 KW.
    \item One sheddable load (type 2, doublepolecontactor-I) with an allowed total reduction of 10\%.
    \item One shiftable load (type 3, pelletizer-II) that can always be shifted and that can have a 15\% loss.
\end{itemize}

The results can be seen in the figures \ref{fig:c3-min}, \ref{fig:c3-max} and \ref{fig:c3-flex}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c3-min.png}
         \caption{Case study 3 - Minimization problem. \label{fig:c3-min}}
\end{figure}

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c3-max.png}
         \caption{Case study 3 - Maximization problem. \label{fig:c3-max}}
\end{figure}

The last configuration has an important contribution from the load of type 3 and a lower one from the load of type 2. That means that the highest degree of freedom in this configuration is the use of the shift of the load of type 3.

\begin{enumerate}
    \item Internal productions don't contribute too much to the minimization and maximization problems, but as in the first configuration the use of the storage is different, with the maximization problem that uses (almost) full charge/discharge cycles.
    \item The sheddable load here has a low possibility of being reduced (only 15\%) and the minimization problem decides to put his production to zero in the last part of the day.
    \item The shiftable load represents the main option of optimization for the POD. The minimization problem aims to always reduce the demand, but to respect the daily conservation (the load can have only a 15\% of loss dureing the day) it has to raise the demand in timestamps 34 to 45. Instead, the maximization problem rises the production even more in the moments of the day in which the production is already really high.
\end{enumerate}

What has been said about the load of type 3 can be clearly see in the results of the flexibilities in figure \ref{fig:c3-flex}. The minimization problem request more energy to the grid in the middle part of the day in order to compensate the global reduction of energy acquired, while the maximization one tends to raise up the moments when the energy requested to the gird is already high.

This leads to a situation where the minimum flexibility is really higher than the maximum one: in the times in which this happens, can be simply considered as a situation in which the POD cannot provide flexibility in those moments of the day.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/c3-flex.png}
         \caption{Case study 3 - Flexibilities. \label{fig:c3-flex}}
\end{figure}

\subsection{Aggregator results}

The result of the aggregation performed by the aggregator is shown in figure \ref{fig:aggregation-result}.

As can be seen, aggregating all the results leads to have two distinct bounds of flexibility. The moments of the day in which the minimum flexibility is higher that the maximum one are considered moments in which the VPP cannot provide flexibility. This condition can happens when a low number of PODs is involved in the optimization, but as has been said at the beginning of the chapter the usually number of PODs is pretty high. The presented use case should be considered a really simple one, but it shows how the proposed model acts as an efficient EMS in a real VPP environment.

In the chapter \ref{chap:5}, the costs related to the flexibility are mentioned. The aggregator should perform an aggregation also on the associated costs in order to have a total cost to be associated to the total flexibility. Despite the model is prepared for having costs and for their optimization, in this practical use case the lack of real and significant data leads to focus on the flexibility rather than the costs.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/agg-res.png}
         \caption{Aggregation result. \label{fig:aggregation-result}}
\end{figure}

As has been said in chapter \ref{chap:5}, the response that the aggregator can give to the TSO changes a lot between use cases. Inside the VIRTUS environment, a possible response can be given by resolving a small optimization problem. The flexibility that can be given can be modelled as a variable that must be between the two bounds identified by the aggregated flexibility calculated in the aggregator. The optimization should minimize the distance between the requested flexibility from the TSO and the variable modelled.