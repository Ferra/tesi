\chapter{Virtual Power Plant}
Due to the growing environment consciousness, renewable energy technologies have caught a lot of attention in the last years. Photovoltaic and wind power based systems are now widely spread worldwide, also thanks to various investments and incentives provided by governments. Moreover, the progressive electricity market liberalization encourage the transition form monopolistic power companies to decentralized systems participating in energy markets, responsible of planning, managing and operating the distribution of energy form different sources. In this context, the integration of distributed energy generation, controllable demand and energy market operations is not simple: this is where the concept of \textit{Virtual Power Plant} (VPP) comes to life.

\section{Concept}
The main idea behind this concept is to aggregate the capacity of managing many \textit{Distributed Energy Resources} (DER) in order to create a single operating profile. Individual DERs would gain visibility, maximizing their revenue opportunities, and the system would benefit from an optimal use of the available resources, reducing the costs, and improving the efficiency of operations \cite{ruiz_direct_2009}.

Moreover, the capability of a VPP of participating in energy markets gives the opportunity of satisfy unforeseen demands buying energy from the market and also the opportunity of don't waste exceeding energy produced by DERs by selling it to the market, creating also a revenue in terms of money for the VPP.

Considering the power capabilities, the whole set of DERs that compose the VPP can reach the size of one or more nuclear power plant. Nevertheless, the majority of the DERs are renewable power plants: this mean that if the sun is not shining or the wind in not strong enough the VPP will be fed with lower power. For this reason having DERs with different energy sources and technologies is essential, in order to provide compensation to any possible oscillation and to guarantee the power balancing \cite{pandvzic2013virtual, saboori2011virtual}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=\textwidth]{images/vpp.eps}
         \caption{The virtual power plant (on the right) is an aggregate of different energy sources that can be used and controlled remotely to satisfy in the best and the cheapest way the flexibility and energy demand. \\Source: Next Kraftwerke.\label{fig:vpp}}
\end{figure}

\section{Benefits}
Introducing the concept of virtual power plant it has been anticipated that the biggest advantage is the capability of dynamically and quickly adjust the production of the DERs to guarantee the energy demand and the optimal use of the different power sources \cite{lombardi_optimal_2009}. But there are benefits also for the power consumers that uses the VPP: industrial and commercial power consumers can indeed profit from the economic optimization provided based on energy market prices \cite{asano2011market}. In this way, they can limit their power consumption when the electricity price is cheaper, reducing the costs to up to a third\footnote{https://www.next-kraftwerke.com/}.

\section{Flexibility}
One of the keyword talking about VPPs is \textit{flexibility}. The flexibility is the capability of balance the energy request in a quick and versatile way. That's one of the greatest strengths of virtual power plants and one of the most notable difference compared to conventional power plants. VPPs can react not only to the unforeseen demands or the failure of some DER, but also to the changes of the electricity price on the energy exchange, executing trades. After all, the price of electricity changes constantly and this can represent a great opportunity for the VPPs \cite{thavlov2014utilization}.

The flexibility can be provided in different ways in a virtual power plant: can be represented by an optimal use of the internal resources with the aim of reducing the total operational costs (for example storing energy in the storage when the energy price is low to use it later when the price is higher) or can be represented, for example, as the interaction of the VPP with the external market.

\section{Components}
As has been said, the VPP is an aggregate of multiple DERs, most of the time renewable ones, that work together and with the market in order to satisfy the energy demand.

Besides the different type of DERs, a virtual power plant can also use storage systems to store the energy. This could be really useful when the VPP has to satisfy an unforeseen demand or when the electricity price on the market is high (indeed it could decide to store the exceeding energy and sell it when is more convenient).

The component which is responsible of coordinate the power flows coming from the DERs, control the loads and decide when to use the storages is called \textit{Energy Management System} (EMS) and it is the core of the virtual power plant. Every other component is connected to it via a bidirectional communication: the EMS can both send signals to control each unit and receive information about their current status from them.

The EMS is responsible for all decisions within the VPP. Based on the the actual energy prices, the availability of renewable resources and the actual load request, the EMS decides \cite{aloini2011optimal}: 

\begin{itemize}
    \item how much energy should be produced;
    \item which generators should contribute to the production of
the required energy;
    \item whether surplus energy should be produced and then
stored or sold;
    \item whether controllable loads should be shifted;
    \item whether electrical energy should be sold/bought to/from the grid.
\end{itemize}

Considering that the type of DERs and storage systems can be very different, those will be better discussed in the next sections, when it will be studied some detailed VPP configurations.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/ems.jpg}
         \caption{The EMS is the core of the virtual power plant. It is responsible of managing the internal resources and grating the power balance. \\Source: Hitachi Global.\label{fig:ems}}
\end{figure}

\subsection{Communication infrastructure}
In the virtual power plant each unit is connected directly or indirectly with the EMS so that the control center can receive the information about the actual status of the participant. Therefore, is important to provide the communication infrastructure for data flow and exchange. Several aspects have to be taken into account with this issue. The network should have a local character, be fast enough, provide the minimum delay and have the capacity for extension of new devices. Depending on the number of the users, the structure of the infrastructure should be changed. Too many connection-points with the EMS could slow down the work of the system or can lead to overstress the communication medium. Because of this, some of the data from units should not be sent individually to the EMS system of the VPP. They may be processed instead as a cluster of the similar units. To ensure that the EMS works properly, the received data must have a specified format and being sent using specified protocol. This needs to be achieved to provide compatible information exchange from different units and make it also possible to extend the system. Further details can be found in literature \cite{lombardi_optimal_2009}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/vpp-communication.png}
         \caption{Communication in a virtual power plant \cite{lombardi_optimal_2009}.\label{fig:vpp-communication}}
\end{figure}

\subsection{Photovoltaic}
Photovoltaic power plants, from now also referred as PV, are one of the most common type of DER due to the wide spread use of this technology. The report by \textit{International Energy Agency}\cite{iea_2020} shows how in 2019 the annual capacity of world PV installations have reached a total production of 627GW, with a growth of more of 20\% Year-over-Year, pointing out the constant growing trend related to the photovoltaic market.

Photovoltaic systems consists of an arrangement of several components like solar panels and inverters and aim to convert the sun's radiation, in the form of light, into usable electricity. They range from small, rooftop-mounted or building-integrated systems with capacities from a few to several tens of kilowatts, to large utility-scale power stations of hundreds of megawatts. 

For their own nature, photovoltaic systems are \textit{grid-connected}: in fact, roughly 90 percent of PV systems works together to limit some drawbacks like intermittency issues and limited storage. Joining a virtual power plant means being connected with more and also more far PV systems, reducing problems even more, granting the possibility of storing large quantity of exceeding energy and smartly distributing power when necessary, increasing also the global stability of the whole system.

Not by chance, last year Tesla deployed the first 100 Powerwalls and solar arrays on 50,000 homes in South Australia, creating the biggest virtual power plant in the world, and Hawaii’s first VPP project, launched in 2016, is using Oahu’s large amounts of rooftop solar power to reduce peak demand and stress on the island’s grid\cite{demcak_2019}.

Usually, due to the large presence of this technology, the photovoltaic power plants represents one of the most used internal generation source of a virtual power plant. The uncertainty related to the production can be transformed in a sort of flexibility from the EMS, which can react properly to unforeseen changes in the production.

\subsection{Wind power}

Wind power is the use of wind to provide the mechanical power through wind turbines to turn electric generators, granting a really small impact on the environment. As the Global Energy review report by \textit{IEA} points out in \cite{iea_global_2020}, wind power is the second most used source of renewable energy after the photovoltaic and is expected to increase the most in absolute generation terms among all renewables.

As the PV systems, also wind power systems are grid-connected for their own nature: wind turbines are in fact connected with each other (creating the so-called \textit{wind farms}) and can use the electric power transmission network.

The wind, that is the core of this kind of systems, is an intermittent energy source and gives a really variable power; that means that a DER based on this system can improve a lot his efficiency thanks to his membership to the VPP.

Nevertheless, as it happens with the photovoltaic, having unexpected peaks leaves the possibility to the EMS to react in a strategic way, for example storing the exceeding energy to use it when it needs or when the production is lower that the expected one.

\subsection{Combined Heat and Power}

The combined heat and power (CHP) systems, also usually called \textit{cogeneration}, rely on the use of a heat engine or power station to generate electricity and useful heat at the same time. The aim of those systems is recover the thermal energy that otherwise will be wasted for heating. As can be seen in figure \ref{fig:chp}, cogenerations systems can reach a really high efficiency compared to the conventional generation, reducing the energy demand by 36\%\footnote{Source: \textit{ASUE}, Arbeitsgemeinschaft für Sparsamen und Umweltfreundlichen Energieverbrauch, Association for the Efficient and Environmentally Friendly Use of Energy. Association that aim to assist the development and the production of energy-saving and eco-friendly technologies}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/Cogeneration.png}
         \caption{Diagram comparing losses from conventional generation vs. cogeneration. \\Source: ASUE. \label{fig:chp}}
\end{figure}

CHP systems are mainly used in the industry field, such as chemical plants, oil refineries and so on and so forth, as they require large amounts of process heat for specific operations. This heat, which is usually used in the form of steam, can be generated at the typically low pressures used in heating, or can be generated at much higher pressure and passed through a turbine first to generate electricity. In the turbine the steam pressure and temperature is lowered as the internal energy of the steam is converted to work. The lower pressure steam leaving the turbine can then be used for process heat.

Talking about virtual power plants, the corresponding DER of those systems is the so-called \textit{MicroCHP}. The installation is usually less than 5 kW in a house or small business and instead of burning fuel to merely heat space or water, some of the energy is converted to electricity in addition to heat. This electricity can be used within the home or business or can be managed by the EMS in the VPP environment \cite{wiki:cogeneration}.

\subsection{Storage}

Energy Storage Systems, also called ESS, can capture the energy produced at one time for use at a later time. Devices of this type are often called \textit{batteries} or \textit{accumulators}. One of the key concept of ESSs is their capability of storing different type of energy sources in a more efficient, conveniently and often more economically way.

As it has been said in the previous sections, the main limit of renewable DERs is the intermittency and the variable power that they produce, based on external factors as the weather and so on and so forth. The capability of energy storage systems of storing energy can level out imbalances between supply and demand, for example capturing the exceeding energy from other DERs or buying energy from the grid market when the price is low to use it later when the demand is high or to sell it when the electricity price is high.

Using storages in a virtual power plant brings a lot of flexibility in the VPP, because the EMS can decide where and when using it, taking decisions based not only on the current produced energy from all the DERs but also based on the market status in that particular time.

\subsection{Loads}

The term \textit{load} in the virtual power plant context refers to the demand of energy requested to the EMS. Most of the time those requests are expansive and quite predictable (in terms of energy) and come from residential, commercial or industrial areas, but can also come from electric vehicles recharge stations and so on and so forth that request less energy but are much more less predictable.

In recent years, controllable loads management has become a topic for researches. Before that, the consumers manage their own loads to reduce their consumption during peak hours. A controllable load management can shift the load reducing peaks and filling the valleys of the load curve. As shown in \cite{asano2011market}, a controllable load strategy could reduce about 10\% of peak demand of a distributed power system. In the literature, many papers about the load management can be found, as for example \cite{shen2015controllable, kinjyo2012decentralized, xing2012microgrid}.

In a virtual power plant, the load management can be done by the EMS by shifting the load. Shifting a load means essentially move electricity consumption from one time period to another, for example postponing an industrial process to another time.  The idea is that by shifting the load to another time, the returns generated through energy cost savings or DERs participation are greater than the loss of production.

In the next sections different type of loads and shifting will be presented and integrated in the VPP optimization.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/shifts.png}
         \caption{Example of possible shifts of the load. \label{fig:shifts}}
\end{figure}

\subsection{Energy Market - Grid}

One of the core component of a virtual power plant is the energy market, also called \textit{grid}. The process of deciding at each moment if buying or selling energy to the grid in order to satisfy the requested demand is one of the main goal of the EMS. Obviously, buying the energy from the grid has a cost, that can change during the day, while selling it can generate a profit.

As has been said, the management of the interaction with the grid is one of the main task of a virtual power plant. Usually the VPP aims to minimize the costs or maximize the flexibility, and this means that a correct and efficient use of the grid is the key of a good virtual power plant.
