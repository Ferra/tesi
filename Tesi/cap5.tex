\chapter{Model the flexibility}\label{chap:5}

In the previous chapters a realistic model of a virtual power plant has been presented. The task of the EMS was to minimize the total operational costs performing appropriate shifts on the requested load and optimizing the use of the internal resources as the photovoltaic or the CHP production and the storage system.

This chapter will present an evolution of the previous model focused on the concept of flexibility: the aim of this model is to manage the internal resources in order to provide the requested flexibility when requested.

The model will be extended with other DERs such as the wind power, with different type of loads and will introduce the concept of \textit{Point of Delivery} (POD). A point of delivery can be considered as a independent site which owns and manage different type of DERs. The POD can make an internal optimization of his resources and then it communicate with the EMS of the VPP. It can be seen as a small VPP into a large VPP: it can optimize the use of his internal resources and it will be managed by the central EMS.

The role of the central EMS is done by an \textit{aggregator}. It receive the optimizations from the PODs and it can manage them in order to provide some sort of flexibility when requested.

This model will perform a two-stage optimization: the first one is and internal optimization \quotes{per-POD}, the second is performed by the aggregator that gets all the flexibility (and the related costs) of the single PODs and decide how to provide the desired flexibility.

\section{Flexibility}\label{section:flexibility}

In the model presented in chapter \ref{chap:3} the flexibility of the virtual power plant was represented by the capability of the VPP of using the internal resources in a versatile way to compensate the desired load demand. Another parameter that characterized the flexibility was the allowed shift of the load: changing this uncertainty parameter gives to the virtual power plant more ways to use his internal DERs, reducing the overall costs (in the section \ref{section:results} can be seen that higher allowed shifts lead to lower total costs).

But the flexibility can also be modelled in other ways. In particular, in this chapter, the concept of flexibility is intended as a quantity of energy that a \textit{Transmission System Operator} (TSO) like Terna\footnote{Terna is the one of the first grid operator for electricity transmission in Europe and manages the Italian high-voltage transmission grid. For more, see \url{https://www.terna.it/en}} can ask to a virtual power plant. The energy management systems should decide if and how give the desired flexibility to the TSO based on the available flexibility of his internal resources.

Typically, in this kind of scenario is not considered only a single virtual power plant with his internal DERs. In fact, the number of internal resources can be really high (up to some thousand) and they can also be very spaced: this can leads to problems in terms of computation effort and time, making impossible to manage in real time the whole energy flows. That's why the concepts of point of delivery and aggregator are introduced: each POD makes a first flexibility optimization based on his internal resources (in this case up to a few dozen) and sends it to the aggregator, which collects all the local optimized flexibilities form the PODs (up to few thousand), aggregates them and decides how to provide the requested flexibility to the TSO.

\section{Model description}

As has been said, this model relies on many PODs and a central aggregator to provide the requested flexibility.

Each POD can be considered as a stand-alone virtual power plant, with his internal DERs such as photovoltaic, wind and CHP production and his required loads. It can interact with the grid buying and selling energy and his main task is to send to the aggregator his own flexibility. The flexibility of a POD is intended as range of the quantity of energy that the POD has to ask to the grid to satisfy his internal load demand. Each POD will perform two optimizations: the first maximizing the use of the grid, the second minimizing it. Those values of flexibility are sent to the aggregator together with a correlated flexibility cost.

The task of the aggregator is to collect all the flexibilities and the related costs that come form the PODs and sum them in order to obtain at each timestamp the overall available flexibility. After that, the aggregator can answer to TSO's requests of flexibility.

The overall logic is illustrated in figure \ref{fig:model-2}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/model-2.png}
         \caption{PODs and aggregator logic.\\Source: proposal presentation.  \label{fig:model-2}}
\end{figure}

\section{Model implementation}

As for the model described in chapter \ref{chap:4}, the optimization of each POD can be seen as a MILP problem that has to be resolved.

The model is implemented in python, using the Pyomo library. The optimizations are performed with the Gurobi solver (very similar results are obtained using CPLEX solver, both in terms of numbers and time).

The class \texttt{POD} encapsulate the structure of a single point of delivery. Many different profiles (see \ref{subs:profiles} for details) can be added to the POD, each one representing a single POD component.

To each POD object is associated a \texttt{Solver} object: it is responsible of instantiate the model associated to the POD and of resolving it following the specified \texttt{resolve\_method}. It also owns the results of the optimization in his variable \texttt{results}.

The class \texttt{Model} encapsulate a common Pyomo model used for the optimization of each POD. It is transparent to the POD object because the model instances are managed by the solver class, but it is implemented is a general way that can be used with every POD configuration.

Finally, the class \texttt{Aggregator} owns a list of PODs and perform the sum of the optimized flexibilities of the PODs. It stores the resulted aggregated flexibility and can provide the flexibility requested by the TSO.

\subsection{Point of Delivery}

The main structure of the POD class can be seen in figure \ref{fig:pod-uml}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/pod-uml.png}
         \caption{POD class UML. \label{fig:pod-uml}}
\end{figure}

This class identifies a group of profiles that have to be optimized in order to satisfy the requested loads. How each profile can be optimized will be discussed in the following subsections.

The number of profiles that a POD can have is limitless; typically this number is up to ten of few dozen. There can be more profiles of the same type, but only one storage (because it is modelled as a free bounded variable without an input profile). Moreover, for simplicity, each POD should have at least one load profile that needs to be balanced.

To launch the POD optimization process, simply call the \texttt{resolve} method (listing \ref{listing:pod1}): it will instantiate a new Solver instance with the profiles of the POD and it will call the \texttt{resolve} method of the solver, using the \texttt{model\_resolve\_method} specified (by default the variable is set to \texttt{MINIMIZE\_AND\_MAXIMIZE}, that will perform both the optimizations). Optional parameters of the \texttt{resolve} method allow to print the solution, print the graphs of the results and print some Pyomo outputs (useful for debugging).

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def resolve(self, model_resolve_method=ModelResolveMethod.MINIMIZE_AND_MAXIMIZE, print_results=False, print_graphs=False, tee=False, pprint=False):

    self.resolve_method = model_resolve_method
    self.solver = None
    self.solver = Solver(self.profiles)

    self.solver.resolve(model_resolve_method, print_results, tee, pprint)
    
    if print_graphs:
        if model_resolve_method == ModelResolveMethod.MINIMIZE:
            self.print_graph('minimized')
        elif model_resolve_method == ModelResolveMethod.MAXIMIZE:
            self.print_graph('maximized')
        elif model_resolve_method == ModelResolveMethod.MINIMIZE_AND_MAXIMIZE:
            self.print_graph('minimized')
            self.print_graph('maximized')
\end{minted}
\caption{POD resolve method.}
\label{listing:pod1}
\end{listing}

\subsubsection{Profile}\label{subs:profiles}

All the resources related to a POD are modelled as profiles. Similar to the model presented in the previous chapters, a profile is an array of \(n\) values that correspond to the production (or load demand) in each of the \(n\) timestamps. The \texttt{Profile} class encapsulate this array and also specify: the number of timestamps, the \texttt{profile\_type} and the minimum and maximum cost associated to at each timestamp of the profile.

As can be seen in the listing \ref{listing:profile1}, the class override some methods from \texttt{object} in order to simply access values of the \texttt{profile} property, making easier to display the profile array. Due to python nature, the type of the variables passed in methods should not be specified. This allows, for example, to pass a single value instead of an array of \(n\) equal values. The \texttt{setup\_array\_for\_property} method is an helper method that is useful to populate an array starting from a single value, while the \texttt{\_\_setup\_profile\_array} method will help if the length of the passed profile is not equal to the number of timestamps (can be useful for example if the passed profile is sampled each hour while the number of timestamps suggest a sample of fifteen minutes: in this case the value for an hour is repeated four times in order to properly setup the array profile). 

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def __init__(self, l, profile_type = ProfileType.NA, cost_min = 0, cost_max = 0, timestamps = 96):
    self.profile_type = profile_type
    self.timestamps = timestamps
    self.profile = self.__setup_profile_array(l)
    self.cost_min = self.setup_array_for_property(cost_min)
    self.cost_max = self.setup_array_for_property(cost_max)

def __getitem__(self, item):
    return self.profile[item]

def __setitem__(self, key, value):
    self.profile[key] = value

def __repr__(self):
    return self.profile

def __str__(self):
    return self.profile.__str__()

def __setup_profile_array(self, l):
    if self.timestamps % len(l) != 0:
        raise Exception('The number of timestamps must be multiple of the length of passed parameters.')
    if self.profile_type == ProfileType.NA or self.profile_type == ProfileType.ZERO:
        result = [0]*self.timestamps
    else:
        result = np.repeat(l, self.timestamps/len(l))

    return result

def setup_array_for_property(self, value):
    if isinstance(value, (list, np.ndarray)):
        if self.timestamps != len(value):
            raise Exception('{} length is not equal to the number of timestamps.'.format(value))
        return value
    else:
        return [value] * self.timestamps
\end{minted}
\caption{Profile class.}
\label{listing:profile1}
\end{listing}

The \texttt{Profile} class is the parent class for the specific types of profiles. For each of these types, a specific class extend the \texttt{Profile} one: these new classes enrich the base profile with the specific parameters that will be used in the model (parameters will be illustrated in section \ref{subs:model}).

\subsection{Solver}\label{subs:solver2}

The \texttt{Solver} class is created with the array of profiles that belongs to a certain POD. It is responsible for instantiating the model, initializing it with the values of the given profiles, doing the optimizations and storing the corresponding results.

Pyomo provides different ways to initialize the model. To grant the reusability of the class and his capability to instantiate the model with different number and type of profiles, the initialization with python dictionary is chosen. In the Pyomo documentation \cite{pyomo-doc}, a detailed explanation of how this dictionary should be can be found. The structure is not conventional (i.e. it requires nested python dictionaries with \texttt{None} as key) and that's why the methods \texttt{add\_data\_field}, \texttt{add\_array\_data\_field}, \texttt{add\_multidimension\_array\_data\_field} and \texttt{model\_parameters\_setup} are implemented: they helps to create in a transparent way, starting from the profiles, the dictionary that will instantiate the model. A detailed view of some of this methods can be seen in listing \ref{listing:solver1}.

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def add_data_field(self, field_name, data):
    if isinstance(data, list):
        if len(data) < 1:
            raise Exception('{} data with no length'.format(field_name))
        else:
            if isinstance(data[0], (list, np.ndarray)):
                self.add_multidimension_array_data_field(field_name, data, len(data[0]))
            else:
                self.add_array_data_field(field_name, data)
    else:
        self.add_single_field(field_name, data)

def add_single_field(self, field_name, value):
    self.__data_for_instance[None][field_name] = {None: value}

def add_array_data_field(self, field_name, data):
    self.__data_for_instance[None][field_name] = {}
    for i in range(len(data)):
        self.__data_for_instance[None][field_name][i] = data[i]

def add_multidimension_array_data_field(self, field_name, data, nested_dimension):
    self.__data_for_instance[None][field_name] = {}
    for i in range(len(data)):
        for j in range(nested_dimension):
            self.__data_for_instance[None][field_name][(i, j)] = data[i][j]
\end{minted}
\caption{Detail of the methods for the creation of the initializing dictionary.}
\label{listing:solver1}
\end{listing}

The \texttt{resolve} method of the class creates the concrete model instance and initializes it with the created dictionary. Then, it will interpret the way the model should be resolved using the passed variable \texttt{model\_resolve\_method}. The model owns two objective functions, as is illustrated in the section \ref{subs:model}, but  Pyomo permits to \textit{activate} and \textit{deactivate} them. This method choose which objective function has to be active and delegates to the method \texttt{\_\_resolve\_model\_and\_results} the real optimization and the extraction of the results. See listing \ref{listing:solver2} for more details on the implementation.

In the case the model has to be resolved both in minimize and maximize sense, two sequentially optimization will be performed, activating and deactivating properly the model objective functions.

\begin{listing}[!h]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def resolve(self, model_resolve_method: ModelResolveMethod, print_results=False, tee=True, pprint=False):

    if len(self.__profiles) == 0:
        raise Exception('Profiles not provided!')
    else:
        self.__setup_data()

    instance = self.__model.create_instance(self.__data_for_instance)

    if model_resolve_method == ModelResolveMethod.MINIMIZE:

        instance.objective_function_minimize.activate()
        instance.objective_function_maximize.deactivate()
        self.__resolve_model_and_results('minimized', instance, tee, pprint)
        # [...] Print results

    elif model_resolve_method == ModelResolveMethod.MAXIMIZE:

        instance.objective_function_minimize.deactivate()
        instance.objective_function_maximize.activate()
        self.__resolve_model_and_results('maximized', instance, tee, pprint)
        # [...] Print results

    elif model_resolve_method == ModelResolveMethod.MINIMIZE_AND_MAXIMIZE:

        instance.objective_function_minimize.activate()
        instance.objective_function_maximize.deactivate()
        self.__resolve_model_and_results('minimized', instance, tee, pprint)
        instance.objective_function_minimize.deactivate()
        instance.objective_function_maximize.activate()
        self.__resolve_model_and_results('maximized', instance, tee, pprint)
        # [...] Print results
        
def __resolve_model_and_results(self, model_resolve_method, instance, tee, pprint):

    start = time.time()
    solver = pyo.SolverFactory('gurobi', solver_io="python")
    result = solver.solve(instance, tee=tee)
    end = time.time()

    # Check the results and extract the values if the solution is optimal
    if (result.solver.status == SolverStatus.ok) and (
            result.solver.termination_condition == TerminationCondition.optimal):
        # [...] Results extraction into "results" variable
\end{minted}
\caption{Details of the activation/deactivation of the objective functions.}
\label{listing:solver2}
\end{listing}

\subsection{Model}\label{subs:model}

The class \texttt{Model} encapsulate a Pyomo \textit{AbstractModel} that will be concretized by the Solver class with the profiles associated to a single POD.

As for the model described in section \ref{section:model-first}, the set of \(T\) timestamps is created in order to index the variables and the parameters of the model. But differently for the previous, this model can contains more profiles of the same type: that's why variables and parameters needs to be indexed also with the number of the corresponding type of profile.

Let's now discuss how the model can manage the different type of profiles.

\subsubsection{PV}

The \texttt{PV} class encapsulates the behaviour of a photovoltaic profile. This class has an array of values that represent the production for each timestamp, a \textit{size} factor, the values of the allowed shift (in percentage: values equal to zero means that the profile cannot be shifted) and the value of the total shift on the whole time horizon. The size factor is introduced because usually the provided profile is a normalized one: in this case, specifying the size permits to create the proper profile of generation. If the profile of the production is not normalized, then the size factor can just be set to one to preserve the original profile. Parameters of the \texttt{PV} class can be seen in figure \ref{fig:pv-uml}.

By default, the values of the allowed shifts are equal to the 10\% of the current production and the total shift is zero.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.6\textwidth, center]{images/pv-uml.png}
         \caption{PV parameters. \label{fig:pv-uml}}
\end{figure}

In the model, the constraints set the bounds of the shifts and grant that the total shift on the time horizon is respected. See the listing \ref{listing:pv-2} for the constraints.

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
# Shift lb
def pv_shift_lb(m, pv, t):
    return m.pv_shift[pv, t] >= m.pv_min_shift[pv, t]
self.model.pv_shift_lb = pyo.Constraint(self.model.PV, self.model.T, rule=pv_shift_lb)

# Shift ub
def pv_shift_ub(m, pv, t):
    return m.pv_shift[pv, t] <= m.pv_max_shift[pv, t]
self.model.pv_shift_ub = pyo.Constraint(self.model.PV, self.model.T, rule=pv_shift_ub)

# Shift at interval t cannot make the profile lower than zero
def pv_shift_zero(m, pv, t):
    return m.pv_shift[pv, t] >= -m.pv_profiles[pv, t]
self.model.pv_shift_zero = pyo.Constraint(self.model.PV, self.model.T, rule=pv_shift_zero)

# Shift rule
def pv_shift_rule(m, pv):
    return sum(m.pv_shift[pv, t] for t in m.T) == m.pv_total_shift[pv]
self.model.pv_shift_rule = pyo.Constraint(self.model.PV, rule=pv_shift_rule)
\end{minted}
\caption{Constraints on the PV profile model.}
\label{listing:pv-2}
\end{listing}

\subsubsection{Wind}

The wind profiles is very similar to the photovoltaic one. In figure \ref{fig:wind-uml} the required parameters are shown. The size factor allows to work with normalized profiles and the \texttt{min\_shift} and \texttt{max\_shift} values are the bounds for the shift. Constraints are not shown because they are really similar to the one described in listing \ref{listing:pv-2}.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.6\textwidth, center]{images/wind-uml.png}
         \caption{Wind parameters. \label{fig:wind-uml}}
\end{figure}

\subsubsection{CHP}

The CHP can be modelled in two ways: the first is the one described in section \ref{section:chp}, the second is similar to the behaviour of PV and wind profiles. The already proposed model is useful when the CHP needs to be modelled as a free variable, that is when the model can manage entirely the production of the system. In another scenario, the CHP is treated as a profile that can be shifted only in desired intervals with the total shift that must be zero on the time horizon.

To provide a different implementation, the latter model is implemented. The requested parameters can be seen in figure \ref{fig:chp-uml}. \texttt{allowed\_t} is an array that contains the number of all the shifts in which the CHP production can be shifted. When the profile is created, this array is converted into a binary array with one in the timestamps in which the shift is allowed, zero otherwise. In the listing \ref{listing:chp-2} are illustrated the two constraints that grant the shift only in the allowed \(t\)s.

The \texttt{total\_shift} parameter is not requested because it is assumed that the total production should stay the same on all the time horizon.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.6\textwidth, center]{images/chp-uml.png}
         \caption{CHP parameters. \label{fig:chp-uml}}
\end{figure}

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
# Shift lb
def chp_shift_lb(m, chp, t):
    return m.chp_shift[chp, t] >= m.chp_min_shift[chp, t] * m.chp_allowed_t[chp, t]
self.model.chp_shift_lb = pyo.Constraint(self.model.CHP, self.model.T, rule=chp_shift_lb)

# Shift ub
def chp_shift_ub(m, chp, t):
    return m.chp_shift[chp, t] <= m.chp_max_shift[chp, t] * m.chp_allowed_t[chp, t]
self.model.chp_shift_ub = pyo.Constraint(self.model.CHP, self.model.T, rule=chp_shift_ub)
\end{minted}
\caption{These constraints set the bounds on the shift and allow the shift only in the allowed timestamps.}
\label{listing:chp-2}
\end{listing}

\subsubsection{Load}

The behaviour of the load modelled in the previous model is a standard one: the load can be shifted by defined percentage of the predicted demand and in sub-intervals the total shift must be zero. This model can well approximate a residential demand but will not represent realistically industry loads.

For this reason this model presents and implements three types of load that well approximate the behaviour of industry loads.

\paragraph{Load T1}

The first type is a \textit{non controllable load}: the given profile cannot be modified or shifted for all the timestamps. This load profile approximate an industrial continuous load that cannot be interrupted.

Modelling this type of load is very easy: it requires only the profile that will contribute in the power balance constraint.

\paragraph{Load T2}

The second type is for \textit{sheddable loads}: this kind of loads can follow the given profile or can reduce their production in given moments of the day, generating only negative shifts. This means that the total allowed reduction of these loads is always different from zero. 

The class \texttt{LoadT2} described in figure \ref{fig:loadt2-uml} shows how this kind of load is implemented. As for the CHP, the \texttt{allowed\_t} array represent the timestamps in which the load can be reduced. The \texttt{allowed\_reduction} array represent a bound for the instant shift (by default it is a sufficient big number to guarantee that the demand can be shifted to zero), while the \texttt{allowed\_reduction\_total} is the maximum value of the loss of production on the whole time horizon (it is intended as a percentage of the total production and by default it is 25\%).

Listing \ref{listing:loadt2} shows the constraint related to this kind of loads.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.6\textwidth, center]{images/loadt2-uml.png}
         \caption{LoadT2 parameters. \label{fig:loadt2-uml}}
\end{figure}

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
# Shift lb
def l2_shift_lb(m, l2, t):
    return m.load_t2_shift[l2, t] >= -m.load_t2_allowed_reduction[l2, t] * m.load_t2_allowed_t[l2, t]
self.model.l2_shift_lb = pyo.Constraint(self.model.L2, self.model.T, rule=l2_shift_lb)

# Shift at interval t cannot make the profile lower than zero
def l2_shift_zero(m, l2, t):
    return m.load_t2_shift[l2, t] >= -m.load_t2_profiles[l2, t]
self.model.l2_shift_zero = pyo.Constraint(self.model.L2, self.model.T, rule=l2_shift_zero)

# Total allowed shift check
def l2_total_shift(m, l2):
    return -sum(m.load_t2_shift[l2, t] for t in m.T) <= m.load_t2_allowed_reduction_total[l2]
self.model.l2_total_shift = pyo.Constraint(self.model.L2, rule=l2_total_shift)
\end{minted}
\caption{Load type 2 constraints.}
\label{listing:loadt2}
\end{listing}

\paragraph{Load T3}

The last kind of load is for \textit{shiftable loads}, similar to the ones modelled in the previous chapters. The main difference is that the load can be shifted only in allowed timestamps and that the \texttt{total\_shift} must be respected on the whole time horizon and not anymore in sub-periods of time.

The class modelling these loads is shown in figure \ref{fig:loadt3-uml} and the related model constraints in the listing \ref{listing:loadt3}

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.6\textwidth, center]{images/loadt3-uml.png}
         \caption{LoadT3 parameters. \label{fig:loadt3-uml}}
\end{figure}

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
# Shift lb
def l3_shift_lb(m, l3, t):
    return m.load_t3_shift[l3, t] >= m.load_t3_min_shift[l3, t] * m.load_t3_allowed_t[l3, t]
self.model.l3_shift_lb = pyo.Constraint(self.model.L3, self.model.T, rule=l3_shift_lb)

# Shift ub
def l3_shift_ub(m, l3, t):
    return m.load_t3_shift[l3, t] <= m.load_t3_max_shift[l3, t] * m.load_t3_allowed_t[l3, t]
self.model.l3_shift_ub = pyo.Constraint(self.model.L3, self.model.T, rule=l3_shift_ub)

# Shift at interval t cannot make the profile lower than zero
def l3_shift_zero(m, l3, t):
    return m.load_t3_shift[l3, t] >= -m.load_t3_profiles[l3, t]
self.model.l3_shift_zero = pyo.Constraint(self.model.L3, self.model.T, rule=l3_shift_zero)

# Shift rule
def l3_shift_rule_lb(m, l3):
    return sum(m.load_t3_shift[l3, t] for t in m.T) >= -m.load_t3_total_shift[l3]
self.model.l3_shift_rule_lb = pyo.Constraint(self.model.L3, rule=l3_shift_rule_lb)

def l3_shift_rule_ub(m, l3):
    return sum(m.load_t3_shift[l3, t] for t in m.T) <= 0
self.model.l3_shift_rule_ub = pyo.Constraint(self.model.L3, rule=l3_shift_rule_ub)
\end{minted}
\caption{Load type 3 constraints.}
\label{listing:loadt3}
\end{listing}

\subsubsection{Storage}

For the storage, to maintain generality, the model described in section \ref{section:storage} is implemented. The default values for \texttt{max\_charge}, \texttt{initial\_charge}, \texttt{max\_delta\_charge} and \texttt{max\_delta\_discharge} are taken from \cite{conte2020optimization}.

\subsubsection{Power balance}

Considering that the model can have a different number of profiles of the same type, the power balance law is implemented slightly differently from the previous model, as illustrated in listing \ref{listing:pb2}.

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def power_balance(m, t):
    return \
        + sum(m.load_t1_profiles[l1, t] for l1 in m.L1) \
        + sum(m.load_t2_profiles[l2, t] + m.load_t2_shift[l2, t] for l2 in m.L2) \
        + sum(m.load_t3_profiles[l3, t] + m.load_t3_shift[l3, t] for l3 in m.L3) \
        - sum(m.pv_profiles[pv, t] + m.pv_shift[pv, t] for pv in m.PV) \
        - sum(m.wind_profiles[w, t] + m.pv_shift[w, t] for w in m.WIND) \
        - sum(m.chp_profiles[chp, t] + m.chp_shift[chp, t] for chp in m.CHP) \
        + (m.storage_in[t] - m.storage_out[t]) \
        - m.grid[t] \
        == 0
self.model.power_balance = pyo.Constraint(self.model.T, rule=power_balance)
\end{minted}
\caption{Power balance law.}
\label{listing:pb2}
\end{listing}

\subsubsection{Objective functions}

As has been said, this model can be used to resolve both the minimization problem and both the maximization one. Pyomo does not allow to use two objective functions in the same model, but allows to declare it. After the model is created, the objective functions can be \textit{activated} and \textit{deactivated} before performing the optimization. This is done in the solver class as illustrated in section \ref{subs:solver2}.

In the listing \ref{listing:obj2} there is the implementation of the two objective functions. As has been said, this model aims to minimize (or maximize) the use of the grid to provide a certain required flexibility.

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def objective_function_minimize(m):
    return sum(m.grid[t] for t in m.T)
self.model.objective_function_minimize = pyo.Objective(sense=pyo.minimize, rule=objective_function_minimize)

def objective_function_maximize(m):
    return sum(m.grid[t] for t in m.T)
self.model.objective_function_maximize = pyo.Objective(sense=pyo.maximize, rule=objective_function_maximize)
\end{minted}
\caption{Implementation of the objective functions. Before the optimization, one of these has to be \textit{deactivated}.}
\label{listing:obj2}
\end{listing}

\subsection{Aggregator}

The class \texttt{Aggregator} owns the list of PODs considered and is responsible for aggregating their flexibilities and costs. The class also implement a method called \texttt{resolve\_pods\_and\_aggregate} that firstly optimize the PODs and then aggregate their results.

The logic of the response to give to the TSO can be implemented in different ways and can vary a lot based on what the TSO can ask. For this reason, the proposed model leaves the implementation of this response to a future implementation.
