\chapter{VPP model implementation}\label{chap:4}

In this chapter will be presented the implementation of the mathematical model described in chapter \ref{chap:3} and the results compared to the component modeling used in the AIxIA2017's model.

The MILP problem will be implemented using \textit{Pyomo}, a \quotes{Python-based open-source software package that supports a diverse set of optimization capabilities for formulating, solving, and analyzing optimization models} \cite{pyomo}. Pyomo has been chosen because offers the possibility of choosing different type of solvers using the same model. Tests are based on a Public Dataset\footnote{www.enwl.co.uk/lvns} from which the electric load demands and the photovoltaic production forecasts are took. Other parameters, such as the storage replace cost or the CHP startup costs are taken from the literature \cite{weber2018realistic, ju2017two, zhou2016optimal, xu2017factoring, bordin2017linear}.

\section{Dataset description}\label{section:dataset}

The dataset presents both profiles of photovoltaic production and load demand with a time step of 5 minutes. Starting from them, aggregated profiles with timestamp of 15 minutes are created and used as input for the optimization. 

As can be seen in figure \ref{fig:load_bounds}, after the aggregation the most of the electrical consumption occurs in certain parts of the day, presenting peaks as expected. The EMS will try to reduce those peaks keeping the shifted demanded load in the given bounds (for example, in the figure \ref{fig:load_bounds} the maximum allowed shift is the 20\% of the original load).

The dataset provides also photovoltaic production with profiles for different size of PV units but for the same sun irradiance (i.e. the same shape but different amplitude due to the different size of the PV panels used). In this case, the aggregation leads to a peak of production in the middle part of the day, with a consequent need of balancing this source of energy to cover periods of high request of energy in the VPP. In figure \ref{fig:pv} an example of a PV profile is shown.

The electricity hourly prices have been taken from GME\footnote{http://www.mercatoelettrico.org/En/Default.aspx}, the Italian national energy market management corporation, and the diesel cost is taken from the Italian Ministery of Economic Development\footnote{http://dgsaie.mise.gov.it} and is assumed to be constant for all the time horizon (as assumed in \cite{aloini2011optimal} and \cite{espinosa2015dissemination}).

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/load_bounds.png}
         \caption{Example of load demand profile with 20\% of allowed shift. \label{fig:load_bounds}}
\end{figure}

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/pv.png}
         \caption{Example of photovoltaic production with 10\% of uncertainty bounds. \label{fig:pv}}
\end{figure}

\section{Pyomo}

Pyomo (\cite{hart2011pyomo, hart2017pyomo}) was developed by William Hart and Jean-Paul Watson in 2008. A core capability of Pyomo is modeling structured optimization applications. It can be used to define general symbolic problems, create specific problem instances, and solve these instances using commercial and open-source solvers.  Pyomo's modeling objects are embedded within a full-featured high-level programming language providing a rich set of supporting libraries, which distinguishes Pyomo from other algebraic modeling languages.

Pyomo supports an object-oriented style of formulating optimization models, which are defined with a variety of modeling components: sets, scalar and multidimensional parameters, decision variables, objectives, constraints, equations, disjunctions and more. Optimization models can be initialized with python data, and external data sources can be defined using spreadsheets, databases, various formats of text files. Pyomo supports both abstract models, which are defined without data, and concrete models, which are defined with data. In both cases, Pyomo allows for the separation of model and data.

It supports dozens of solvers, both open source and commercial, that can be used to resolve different types of optimization problems, like discrete optimization (MIP) with Gurobi, CPLEX, CBC or GLPK, nonlinear optimization (NLP) with IPOPT or KNITRO,  or mixed-integer nonlinear Optimization (MINLP) with solvers like Baron or COUENNE.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/pyomo.png}
         \caption{Pyomo at a glance. \label{fig:pyomo}}
\end{figure}

\section{Implementation}

The following section will show the implementation of the model described in chapter \ref{chap:3}. As has been said, the implementation is realized in python, using Pyomo library for the optimization model. The two main solvers used are Gurobi and CPLEX, but as highlighted in the results section there is not a notable difference between them.

\subsection{General structure}

The \texttt{model.py} file contains the Pyomo model of the virtual power plant. This model is used by the class \texttt{Solver} (in the \texttt{solver.py} file) that, once created, is responsible of initialize the model and his variables, perform the optimization with the selected solver and encapsulate the results. 

For simplicity, a list of parameters are defined in the \texttt{parameters\_definition.py} file and are used by the solver to populate the model parameters.

\subsection{Model} \label{section:model-first}

As has been said, the file \texttt{model.py} encapsulate a Pyomo \textit{AbstractModel}. The model is populated with all the variables, the parameters and the constraints described mathematically in chapter \ref{chap:3}.

For each variable, an \textit{index set} to make the variable indexed (in this case, the index set is represented by the set of all the timestamps, from zero to ninety five) and a domain that implicitly set some bounds can be specified.

Also the parameters can be indexed (with the timestamp set) and have the domain field, but also optional directives as a default value and a mutable boolean parameter can be specified.

The constraints are implemented as using equality or inequality expressions that are created using a rule, which is a python function. Also constraints can be indexed by lists or sets (in this case, they will be indexed with the set of the timestamps). When the declaration contains lists or sets as arguments, the elements are iteratively passed to the rule function. If there is more than one, then the cross product is sent.

\subsubsection*{Load}

The only load related variable is the quantity of the shifted demand (\texttt{load\_shift}). This is a real variable that represent, at each timestamp, the variation of the shifted load from the original one (\texttt{requested\_load}). \texttt{shifting\_bound} represent the percentage of the allowed shift and \texttt{shifting\_intervals} is the number of the sub-periods in which the sum of the shifted load must be zero (\(T_{n}\) in equation \ref{eq:load5}).

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
model.load_shift = pyo.Var(model.N, domain=pyo.Reals)
model.requested_load = pyo.Param(model.N, domain=pyo.NonNegativeReals, mutable=True)
model.shifting_bound = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.shifting_intervals = pyo.Param(domain=pyo.PositiveIntegers, default=1, mutable=True)
\end{minted}
\caption{Load model.}
\label{listing:load1}
\end{listing}

The constraints related to the load can be seen in the listing \ref{listing:load2}. Upper and lower bounds of the shift are specified, as the shifting rule of the equation \ref{eq:load5}.

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def load_shift_ub(m, t):
    return m.load_shift[t] <= m.requested_load[t]*m.shifting_bound/100
model.load_shift_ub = pyo.Constraint(model.N, rule=load_shift_ub)

def load_shift_lb(m, t):
    return m.load_shift[t] >= -(m.requested_load[t]*m.shifting_bound/100)
model.load_shift_lb = pyo.Constraint(model.N, rule=load_shift_lb)

def shifting_rule(m, i):
    r = int(pyo.value(m.timestamps)/pyo.value(m.shifting_intervals))
    return sum(m.load_shift[t] for t in range(r * i, r * (i + 1))) == 0
model.shifting_rule = pyo.Constraint(model.I, rule=shifting_rule)
\end{minted}
\caption{Load constraints.}
\label{listing:load2}
\end{listing}

\subsubsection*{CHP}

To implement the model described in section \ref{section:chp} for the CHP, one variable and many parameters should be specified. 

In the listing \ref{listing:chp1} are defined all the parameters specified in equations from \ref{eq:chp1} to \ref{eq:chp6}. Also some helper variables are defined: \texttt{chp\_status} is a boolean variable that is 0 if the CHP is off, 1 otherwise; \texttt{chp\_shutting\_up} and \texttt{chp\_shutting\_down} take the value 1 if the CHP is respectively starting or shutting down during the current timestamp.

The constraints related to the CHP define the upper and lower bound for the production, the ramp up and ramp down characteristic described by equations \ref{eq:chp2} and \ref{eq:chp3}, the operational limits granted by equation \ref{eq:chp4} and \ref{eq:chp5} and the CHP status definition. Some of those constraints are illustrated in the listing \ref{listing:chp2}.

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
model.chp = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.chp_min = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.chp_max = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.chp_minimum_step_up = pyo.Param(domain=pyo.NonNegativeIntegers, default=0, mutable=True)
model.chp_minimum_step_down = pyo.Param(domain=pyo.NonNegativeIntegers, default=0, mutable=True)
model.chp_ramp_up_limit = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.chp_ramp_down_limit = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.chp_startup_cost = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.chp_diesel_cost = pyo.Param(model.N, domain=pyo.NonNegativeReals, mutable=True)
model.chp_initial_output_power = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.chp_initial_status = pyo.Param(domain=pyo.Binary, default=0, mutable=True)
model.chp_status = pyo.Var(model.N, domain=pyo.Binary)          # 0: down, 1: up
model.chp_starting_up = pyo.Var(model.N, domain=pyo.Binary)
model.chp_shutting_down = pyo.Var(model.N, domain=pyo.Binary)
model.chp_step_initial_power = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.chp_step_final_power = pyo.Var(model.N, domain=pyo.NonNegativeReals)
\end{minted}
\caption{CHP parameters.}
\label{listing:chp1}
\end{listing}

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def chp_ramp_up(m, t):
        return m.chp_step_final_power[t]-m.chp_step_initial_power[t] <= m.chp_ramp_up_limit
model.chp_ramp_up = pyo.Constraint(model.N, rule=chp_ramp_up)

def chp_ramp_down(m, t):
        return m.chp_step_initial_power[t] - m.chp_step_final_power[t] <= m.chp_ramp_down_limit
model.chp_ramp_down = pyo.Constraint(model.N, rule=chp_ramp_down)

def chp_min_step_up_constraint(m):
    return sum(m.chp_status[t] for t in m.N) >= m.chp_minimum_step_up
model.chp_min_step_up_constraint = pyo.Constraint(rule=chp_min_step_up_constraint)

def chp_min_step_down_constraint(m):
    return sum(1-m.chp_status[t] for t in m.N) >= m.chp_minimum_step_down
model.chp_min_step_down_constraint = pyo.Constraint(rule=chp_min_step_down_constraint)

def chp_status_definition(m, t):
    return (0, -m.chp[t]+m.chp_max*m.chp_status[t], m.chp_max-1)
model.chp_status_definition = pyo.Constraint(model.N, rule=chp_status_definition)
\end{minted}
\caption{CHP constraints.}
\label{listing:chp2}
\end{listing}

\subsubsection*{Grid}

The exchange of energy with the market is described with two variables that can fluctuate between given bounds (upper and lower bound constraints are not shown). Associated costs for each timestamp are given, and the constraint illustrated in listing \ref{listing:grid1} ensure the mutually exclusion on buying from and selling to the grid (meaning that for each \(t\) the VPP choose to sell \textit{OR} to buy, it cannot do both at the same time). 

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
model.grid_in = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.grid_out = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.grid_in_min = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.grid_in_max = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.grid_out_min = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.grid_out_max = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.grid_in_cost = pyo.Param(model.N, domain=pyo.NonNegativeReals, mutable=True)
model.grid_out_cost = pyo.Param(model.N, domain=pyo.NonNegativeReals, mutable=True)

def grid_in_out_rule(model, t):
    return model.grid_out[t]*model.grid_in[t] == 0
model.grid_in_out_rule = pyo.Constraint(model.N, rule=grid_in_out_rule)
\end{minted}
\caption{Grid model implementation.}
\label{listing:grid1}
\end{listing}

\subsubsection*{Storage}

The implementation of the storage model described in section \ref{section:storage} requires the definition of nine parameters. Moreover, the parameter \texttt{storage\_initial\_charge} is used at the first timestamp. As listing \ref{listing:storage1} displays, some helping variables are declared. Related to the storage system, two variables are also declared, \texttt{storage\_in} and \texttt{storage\_out}, that represent respectively the the energy that the storage puts in the VPP (while discharging) and the energy that come to the storage (while charging).

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
model.storage_in = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.storage_out = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.storage_initial_charge = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_max_charge = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_min_charge = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_delta_charge = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_delta_discharge = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_charging_eff = pyo.Param(domain=pyo.NonNegativeReals, default=0.95, mutable=True)
model.storage_discharging_eff = pyo.Param(domain=pyo.NonNegativeReals, default=0.95, mutable=True)
model.storage_replacement_cost = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_lifetime_throughput = pyo.Param(domain=pyo.NonNegativeReals, default=0, mutable=True)
model.storage_roundtrip_eff = pyo.Param(domain=pyo.NonNegativeReals, default=0.95, mutable=True) 
# Helper variables
model.storage_step_initial_charge = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.storage_step_final_charge = pyo.Var(model.N, domain=pyo.NonNegativeReals)
model.storage_charging = pyo.Var(model.N, domain=pyo.Binary)
model.storage_discharging = pyo.Var(model.N, domain=pyo.Binary)
\end{minted}
\caption{Storage model parameters.}
\label{listing:storage1}
\end{listing}

\begin{listing}[!htb]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def charging_discharging_const(m, t):
    return m.storage_charging[t] + m.storage_discharging[t] <= 1
model.charging_discharging_constraint = pyo.Constraint(model.N, rule=charging_discharging_const)

def storage_in_bound(m, t):
    return m.storage_in[t] <= m.storage_delta_discharge*m.storage_discharging[t]
model.storage_in_bound = pyo.Constraint(model.N, rule=storage_in_bound)

def storage_in_bound_2(m, t):
    return m.storage_in[t] <= m.storage_delta_discharge*(1-m.storage_charging[t])
model.storage_in_bound_2 = pyo.Constraint(model.N, rule=storage_in_bound_2)

def storage_out_bound(m, t):
    return m.storage_out[t] <= m.storage_delta_charge*m.storage_charging[t]
model.storage_out_bound = pyo.Constraint(model.N, rule=storage_out_bound)

def storage_out_bound_2(m, t):
    return m.storage_out[t] <= m.storage_delta_charge*(1-m.storage_discharging[t])
model.storage_out_bound_2 = pyo.Constraint(model.N, rule=storage_out_bound_2)

model.storage_cost = model.storage_replacement_cost/(model.storage_lifetime_throughput*model.storage_roundtrip_eff)
\end{minted}
\caption{Storage constraints.}
\label{listing:storage2}
\end{listing}

The constraints (some of them can be seen in the listing \ref{listing:storage2}) set the bounds for the SoC of the battery, the bounds on charging and discharging (with related efficiencies) and set the law of the storage described in the equation \ref{eq:st1}. The constant \texttt{storage\_cost} will be used in the objective function as the cost related to the storage.

\subsubsection*{Power balance}

The power balance law of the equation \ref{eq:pb1} is implemented in the listing \ref{listing:pb1} by adding a constraints to the model. The shifted load is represented as the sum of the requested load and the planned shift.

\begin{listing}[ht]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def power_balance(m, t):
    return m.requested_load[t]+m.load_shift[t] == m.chp[t] + m.pv[t] + m.grid_in[t] - m.grid_out[t] + m.storage_in[t] - m.storage_out[t]
model.power_balance_constraint = pyo.Constraint(model.N, rule=power_balance)
\end{minted}
\caption{Power balance law.}
\label{listing:pb1}
\end{listing}

\subsubsection*{Objective function}

The objective function is implemented with the \texttt{Objective} function of Pyomo. As for constraints, a rule (python function) declare the desired objective function and the \texttt{sense} option is used to specify if the problem is a minimization or maximization one (if omitted, by default a minimization problem is considered).

Implementation can be seen in listing \ref{listing:obj1}.

\begin{listing}[ht]
\begin{minted}
[
frame=lines,
framesep=2mm,
baselinestretch=1.2,
fontsize=\scriptsize,
linenos,
breaklines
]{python}
def objective_function(m):
    return sum(m.grid_in_cost[t]*m.grid_in[t] + m.chp_diesel_cost[t]*m.chp[t] + m.chp_starting_up[t]*m.chp_startup_cost + m.storage_cost*m.storage_out[t] - m.grid_out_cost[t]*m.grid_out[t] for t in m.N)
model.objective_function = pyo.Objective(rule=objective_function, sense=pyo.minimize)
\end{minted}
\caption{Objective function.}
\label{listing:obj1}
\end{listing}

\section{Results and discussion}\label{section:results}

In this section the results of the proposed model are shown, showing a comparison between the achieved results and a modeling of the components similar to the one used in the AIxIA2017's model. After the new cost modeling for the storage and the CHP a rise in the minimized costs is expected. Nevertheless, is expected that the solutions follow the trend of the previous one. It is also expected that the optimization problem will not take too much time to being resolved.

The dataset used is the one described in section \ref{section:dataset}; storage and CHP parameters are taken from the cited literature.

After the simulation on over 100 input realizations, the results of three different scenarios are presented, each one with similar PV production and load demand but with different parameters for storage and CHP parameters. The first one uses medium size storage capacity and low CHP production, the second one uses a bigger storage and a CHP that should stay off for half a day and the third one uses medium size storage and CHP production but also has a smaller PV production. The case of a great production of CHP is not tested because in the previous model the associated cost depends only from the diesel cost, which is really low, leading to an intensive use of this kind of production all day long.

The following use cases use a PV profile and a load profile from the dataset described in section \ref{section:dataset}. In addition, each of them has a different configuration for storage and CHP parameters, with realistic data taken from the cited literature. The table \ref{tab:configs} collects the value of the parameters for the used use cases.

\begin{table}[!htb]
\centering
\begin{tabular}{|c|c|c|c|} 
\hline
Case study & \begin{tabular}[c]{@{}c@{}}Storage capacity\\(KW)\end{tabular} & \begin{tabular}[c]{@{}c@{}}CHP capacity\\(KW)\end{tabular} & \begin{tabular}[c]{@{}c@{}}CHP minimum number\\of intervals OFF\end{tabular}  \\ 
\hline
CS1        & 200                                                            & 100                                                        & 20                                                                            \\ 
\hline
CS2        & 400                                                            & 150                                                        & 48                                                                            \\ 
\hline
CS3        & 250                                                            & 120                                                        & 4                                                                             \\
\hline
\end{tabular}
\caption{Configurations of the tested case studies. Each of them also have a PV and a load profile defined in the described dataset.}
\label{tab:configs}
\end{table}

For each use case the Gurobi\footnote{https://www.gurobi.com/} solver is used. It has been tested also with the CPLEX solver, but results does not present significan differences.

\subsection{Case study 1}

The fist case study uses a storage of 200 KW and a CHP that can produce at maximum 100 KW and must be switched off for at least 20 timestamps. The figure \ref{fig:cs1-input} shows the input profiles for the first case study.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.7\textwidth, center]{images/cs1-input.png}
         \caption{Case study 1: input profiles. \label{fig:cs1-input}}
\end{figure}

\begin{comment}
\begin{table}[!htb]
\centering
%\refstepcounter{table}


\begin{tabular}{|c|c|c|c|c|c|} 
\toprule
\multicolumn{1}{c}{}                              & \multicolumn{2}{c}{Previous Model}                                                                      & \multicolumn{2}{c}{Proposed Model}                                                                      & \multicolumn{1}{c}{}                                      \\
\begin{tabular}[c]{@{}c@{}}shift\\\%\end{tabular} & \begin{tabular}[c]{@{}c@{}}costs\\(K€)\end{tabular} & \begin{tabular}[c]{@{}c@{}}time\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}costs\\(K€)\end{tabular} & \begin{tabular}[c]{@{}c@{}}time\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}difference\\(\%)\end{tabular}  \\ 
\midrule
5                                                 & -411.153                                            & 0.26                                              & -292.651                                            & 0.48                                              & -28.82                                                    \\
10                                                & -415.019                                            & 0.28                                              & -296.743                                            & 0.43                                              & -28.50                                                    \\
15                                                & -419.057                                            & 0.26                                              & -300.911                                            & 0.40                                              & -28.19                                                    \\
20                                                & -423.069                                            & 0.28                                              & -305.130                                            & 0.53                                              & -27.88                                                    \\
25                                                & -427.111                                            & 0.28                                              & -309.512                                            & 0.46                                              & -27.53                                                    \\
\bottomrule
\end{tabular}
\caption{Costs and \% difference among the models with different allowed shifts.}
\label{table:cs1}
\end{table}
\end{comment}

In the figure \ref{fig:cs1-res} are shown the results of the optimization with the AIxIA2017 modeling of the components (left) and with the proposed model (right): not considering constraints on CHP and storage leads to an extensive use of them. Due to the low cost of the diesel, the VPP try to use it all day long instead of buying energy from the grid. 

The most interesting case is the use of the storage: without considering the price correlated to the charging and discharging cycles, the AixIA2017's model makes an intense use of it, reducing the intrinsic battery capacity. As has been said in section \ref{section:storage}, modelling a realistic wear for the storage due to charging and discharging cycles is too related to the type of the storage. The proposed model, at the same time, keeps some generality and guarantee a correct and more realistic usage of the storage systems.

\begin{figure}[!htb]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/cs1-res-al.png}
  \caption{AIxIA2017 model results.}
  \label{fig:c1-sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/cs1-res.png}
  \caption{Proposed model results.}
  \label{fig:c1-sub2}
\end{subfigure}
\caption{Comparison between the results, both considering 20\% of shift allowed.}
\label{fig:cs1-res}
\end{figure}

\subsection{Case study 2}

The second case study uses a storage of 400 KW and a CHP that can produce at maximum 150 KW and must be switched off for at least half a day (48 timestamps). The figure \ref{fig:cs2-input} shows the input profiles for the second case study.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.7\textwidth, center]{images/cs2-input.png}
         \caption{Case study 2: input profiles. \label{fig:cs2-input}}
\end{figure}

\begin{comment}
\begin{table}[!htb]
\centering
\begin{tabular}{|c|c|c|c|c|c|} 
\toprule
\multicolumn{1}{c}{}                              & \multicolumn{2}{c}{Previous Model}                                                                      & \multicolumn{2}{c}{Proposed Model}                                                                      & \multicolumn{1}{c}{}                                      \\
\begin{tabular}[c]{@{}c@{}}shift\\\%\end{tabular} & \begin{tabular}[c]{@{}c@{}}costs\\(K€)\end{tabular} & \begin{tabular}[c]{@{}c@{}}time\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}costs\\(K€)\end{tabular} & \begin{tabular}[c]{@{}c@{}}time\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}difference\\(\%)\end{tabular}  \\ 
\midrule
5                                                 & -703.643                                            & 0.26                                              & -271.826                                            & 0.53                                              & -61.37                                                    \\
10                                                & -707.664                                            & 0.26                                              & -276.053                                            & 0.57                                              & -60.99                                                    \\
15                                                & -711.669                                            & 0.24                                              & -280.397                                            & 0.55                                              & -60.60                                                    \\
20                                                & -715.739                                            & 0.28                                              & -284.740                                            & 0.58                                              & -60.22                                                    \\
25                                                & -719.797                                            & 0.22                                              & -289.084                                            & 0.64                                              & -59.84                                                    \\
\bottomrule
\end{tabular}
\caption{Costs and \% difference among the models with different allowed shifts.}
\label{table:cs2}
\end{table}
\end{comment}

The fact that the AIxIA2017 model does not have a cost associated to the battery, allows a really extensive use of it (as can be seen in figure \ref{fig:c2-sub1}) especially in the first part of the day when there isn't PV production. The proposed model uses the storage in a more strategic way, charging it in the first part of the day and using it in the last part, when the load demand is high and the PV production is low.

\begin{figure}[!htb]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/cs2-res-al.png}
  \caption{AIxIA2017 model results.}
  \label{fig:c2-sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/cs2-res.png}
  \caption{Proposed model results.}
  \label{fig:c2-sub2}
\end{subfigure}
\caption{Comparison between the results, both considering 20\% of shift allowed.}
\label{fig:cs2-res}
\end{figure}

\subsection{Case study 3}

The third case study uses a storage of 250 KW and a CHP that can produce at maximum 120 KW and must be switched off for at least 4 timestamps. The figure \ref{fig:cs3-input} shows the input profiles for the third case study. In this case the PV production is lower compared to the previous ones, with a peak of maximum 600KW.

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/input-cs3.png}
         \caption{Case study 3: input profiles. \label{fig:cs3-input}}
\end{figure}
\begin{comment}
\begin{table}[!htb]
\centering
\begin{tabular}{|c|c|c|c|c|c|} 
\toprule
\multicolumn{1}{c}{}                              & \multicolumn{2}{c}{Previous Model}                                                                      & \multicolumn{2}{c}{Proposed Model}                                                                      & \multicolumn{1}{c}{}                                      \\
\begin{tabular}[c]{@{}c@{}}shift\\\%\end{tabular} & \begin{tabular}[c]{@{}c@{}}costs\\(K€)\end{tabular} & \begin{tabular}[c]{@{}c@{}}time\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}costs\\(K€)\end{tabular} & \begin{tabular}[c]{@{}c@{}}time\\(s)\end{tabular} & \begin{tabular}[c]{@{}c@{}}difference\\(\%)\end{tabular}  \\ 
\midrule
5                                                 & 61.437                                              & 0.33                                              & 112.259                                             & 0.64                                              & 82.72                                                     \\
10                                                & 57.520                                              & 0.21                                              & 108.371                                             & 0.75                                              & 88.41                                                     \\
15                                                & 53.585                                              & 0.25                                              & 104.457                                             & 0.55                                              & 94.94                                                     \\
20                                                & 49.612                                              & 0.21                                              & 100.512                                             & 0.59                                              & 102.60                                                    \\
25                                                & 45.529                                              & 0.26                                              & 96.470                                              & 0.69                                              & 111.89                                                    \\
\bottomrule
\end{tabular}
\caption{Costs and \% difference among the models with different allowed shifts.}
\label{table:cs3}
\end{table}
\end{comment}

Having a lower PV production and a large battery capacity leads to an extensive use of the storage system in the AIxIA2017's model. The proosed model keeps using the storage a lot to supply the low PV production but in a more wisely way, avoiding fast and total charge/discharge cycles.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/cs3-res-al.png}
  \caption{AIxIA2017 model results.}
  \label{fig:c3-sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/cs3-res.png}
  \caption{Proposed model results.}
  \label{fig:c3-sub2}
\end{subfigure}
\caption{Comparison between the results, both considering 20\% of shift allowed.}
\label{fig:cs3-res}
\end{figure}

\subsection{Final considerations}

In all the case studies larger percentage of allowed shift leads to larger earning in terms of money. The proposed model does not change this trend, although the costs increase (respect to an implementation using the AIxIA2017 modeling of the components) due to the consideration of the battery and CHP realistic costs.

The proposed model is more complex compared to the AIxIA20017 one and this leads to an increase in the time taken by the solver to solve the model. Although, the resolution time is always way lower than a second, so this should not be a problem.

Thanks to Pyomo, the proposed model has been tested with different solvers live Gurobi and CPLEX. Although, the results of the optimization and the time taken are very similar. For this reason the comparison is not illustrated here.

The more realistic aspect can be seen in the use of both the CHP and the storage. The first now respects the time constraints that can be given and respects the typical stepped profile that characterize it. Talking about the latter, the VPP now try to avoid fast charge/discharge cycles that in the reality can compromise the life of the battery.

Further optimization can be done with more accurate and specific models, especially for the storage (some examples can be found in the literature \cite{ju2017two, zhou2016optimal, xu2017factoring, bordin2017linear}), but doing this causes the loss of generality that this model grants.

