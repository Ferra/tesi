\chapter{VPP robust model}\label{chap:3}

The aim of this chapter is to provide a first robust and mathematical approximation of a virtual power plant. Starting from the model described in \cite{defilippo_offline_2018} and \cite{defilippo_optimization_2017} (cited as \textbf{AIxIA2017} from now on), a more detailed model for CHP and storage will be illustrated.

In these two papers a two-step optimization is proposed. The first is an off-line optimization followed by an on-line one based (with a simple anticipatory method and with a given heuristic). Both the optimization aims to the minimization of operational costs ensuring the energy balance at each point in time. Both the papers use a really simple model that can be enhanced for example in the storage and CHP modeling, considering constraints and costs that are well known in the literature.

As has been said, the key task of a virtual power plant is trying to optimize the use of renewable sources of the VPP to satisfy in each moment the load demand, with the possibility of working with the grid buying and selling energy when necessary. This is performed by the EMS, that has actually to perform an optimization problem.

This thesis aims to improve the modeling of the components of AIxIA2017's model, focusing on the day-ahead optimization in order to provide an optimal and realistic use of the internal resources.

\section{Optimization under uncertainty}

Make decisions without a complete knowledge about the problem data is the core of optimization under uncertainty. This situation is extremely common and at the same time very challenging: in fact, the optimization should be done for every possible contingency, which is often impossible or impractical \cite{powell2016unified}.

Assuming that all parameters are deterministic is a frequent used method, but this is recommended to use only if the impact of uncertainty is negligible \cite{sahinidis2004optimization}. In a different situation, like in a virtual power plant, using stochastic approach becomes necessary (see \cite{shapiro2007tutorial} for an introduction or \cite{birge2011introduction, kall1994stochastic} for an extensive discussion).

Data subject to uncertainty can often be represented via \textit{random variables in a multi-stage decision system}. After taking the decision for a stage a random event occurs, for example some of the random variables are instantiated, and the decision for the next stage must be taken, and so on. As \cite{Shapiro2013, defilippo_offline_2018} point out, use sampling to approximate the probability distribution of the random variables is a very common and yields a certain number of scenarios, each of these with a specific set of decisions. Obviously, more scenarios lead to a better approximation and improve the estimation quality but require a recursively procedure that lead to a much larger computation time, with major impacts on the solution time.

\section{Optimization in a VPP}

Concerning about optimization problem in a virtual power plant environment there is something more to consider compared to a normal optimization problem. In fact in a VPP there is a large factor of uncertainty, related both to the energy production of renewable DERs and the requested load along the day. 

Optimization under uncertainty are challenging to solve, especially if high quality and robust solutions are desirable. For this reason, they are traditionally solved via off-line methods \cite{defilippo_offline_2018}. Optimizing a VPP problem off-line means resolving a day-ahead problem using the predictions for the following day of DERs' production and load's demand and using a robust approach. However, a virtual power plant should quickly and correctly react to unforeseen variations in production or demand and this is why an on-line algorithm to make decisions over time (and without a complete knowledge of the future) is mostly of the time required in a VPP optimization problem.

As \cite{defilippo_offline_2018} points out, both approaches make sense: \quotes{strategic} decisions can be taken off-line, while \quotes{operational} decisions are better left to an on-line approach. This means that the EMS should perform a day-ahead off-line optimization, planning the use of renewables DERs, the shift of loads and the use of the storage, and then it should guarantee the energy balance during the next day using the on-line algorithm and the off-line optimization that it done the day before.

The AIxIA2017's model implements these strategies but modelling the components in a very simple and poor realistic way. The proposed model is going to focus on the day-ahead optimization, considering the technical constraints of some key components as the storage and the CHP system. The proposed model can then be used as a starting point for further researches and could be used to test the optimizations proposed in the AIxIA2017's model.

\section{Model description}

A virtual power plant can have different DERs, both in numbers and types. For example \cite{pandvzic2013offering} proposes a model with a wind power plant, a conventional power plant and a pumped hydro power plant. \cite{ju2016bi} uses a model with a wind power plant, a photovoltaic generator, a conventional gas turbine, an energy storage system and some demand resource providers.

The model implemented in this thesis aims to improve the modeling of the component proposed in \cite{defilippo_offline_2018, defilippo_optimization_2017} using a photovoltaic production, a shiftable load and adding a more realistic characterization for CHP and storage. It is assumed that the distribution of the random variable is available for sampling, as it is often the case when historical data or a predictive model is available. Also, it is assumed that the distribution of the random variables is independent of current decision; this assumption holds in a great variety of applications and has significant computational advantages.

In this first model, the result of the optimization will produce an optimized profile of the shifted demand by assuming as input the profile of the predictions for the PV generation, the profile of the load demand, a fixed percentage of allowed demand shift and the parameters representing the CHP and the storage. 

The optimization problem will be modeled via Mixed Integer Programming (MILP) formulations.

\subsection{Profiles}

All the flow of energy, from the DERs, form the loads or from the grid, are represented as profiles. A profile is a set of \(n\) values that represent the \textit{baseline} of the profile. Each \(n\)-value represent the energy production (or the load demand) in a \(n\)moment of the day. This means that the bigger the value of \(n\) the more the profile is sampled during the day. In this model is assumed a value of \(n\) equal to 96: in so doing the day is divided in intervals of fifteen minutes and each value of the set will correspond to the production (or load) at the given interval of time.

Associated to each profile, a bound of uncertainty is considered. The considered ranges of uncertainty is based on the same assumption of AIxIA20017 (i.e. for the production resources a range of +-10\% is assumed, while for the load demands the range is +-200\%). The decision variable of the model is going to move inside those bounds in order to optimize the objective function. 

\begin{figure}[!htb]
        \vspace*{1cm}
  		\includegraphics[width=.8\textwidth, center]{images/pv_profile.png}
         \caption{Example of the baseline of a profile (in this case a photovoltaic one). The profile can be represented as an array of 96 values, corresponding at the output power of the PV during the day. \label{fig:profile}}
\end{figure}

%\subsection{Robust approach to model uncertainty}

%To model in a robust way the uncertainty with reference to the prediction error on the PV generation and to the uncontrollable deviations from the planned demand, for each of these quantities the range of uncertainty is specified via a lower and an upper bound (for each timestamp) which can be obtained for example by estimating confidence intervals. These bounds are used to define a limited number of scenarios to calculate the optimized demand shifts that minimizes the expected daily operation costs.

%These optimized demand shifts will be then given as input in the on-line part of the algorithm to react properly to unforeseen variations as mentioned in sections above.

%For this part, to highlight the differences with the previous proposed model, we consider only the scenario in which the predicted photovoltaic profile has no uncertainty, considering uncertainty on the load as the bounds in which the shifted load can be.

\subsection{CHP}\label{section:chp}

As mentioned in \cite{defilippo_optimization_2017}, in the previous proposed model the decisions regarding the CHP was treated as independent, assuming that each timestamp is long enough to decide independently from the previous timestamp if switching on or off the CHP generation. The main goal is to model a realistic CHP system with his main characteristics but keeping a general point of view that leads to specific CHP implementations when needed in specific cases.

A way more realistic model is proposed by \cite{weber2018realistic}. However, the model presented in this research is strictly related to real German CHP system, considering many factors as the \textit{thermal inertia} or the \textit{efficiency of the electrical part of the CHP} that, if considered, will make the model less general. Therefore, only the really characteristic behaviours will be modelled in this solution.

First of all, the bounds of CHP power at each timestamp have to be set.

\begin{equation}\label{eq:chp1}
    P_{CHP}^{min} \leq P_{CHP}(t) \leq P_{CHP}^{max}\;\;\;\;\;\;\forall t\in T
\end{equation}

Equation \ref{eq:chp1} grants that the current power of the CHP at the timestamp \(P_{CHP}(t)\) can fluctuate between nominal limits of the CHP (with \(T\) being the set of all timestamps).

One of the main traits of CHP systems is that their power profiles follow a typical \quotes{stepped} line. In the previous model this was not modelled and the variable could fluctuate without restrictions between the bounds. The steepness of the profile can vary a lot between different implementations and can be affected by different parameters like efficiency and others: to keep a more general model possible, a fixed limit for the \quotes{ramp} is proposed. 

\begin{equation}\label{eq:chp2}
    P_{CHP}(t+1) - P_{CHP}(t) \leq P_{CHP}^{ramp\_up}\;\;\;\;\;\;\forall t\in T
\end{equation}
\begin{equation}\label{eq:chp3}
    P_{CHP}(t) - P_{CHP}(t+1) \leq P_{CHP}^{ramp\_down}\;\;\;\;\;\;\forall t\in T
\end{equation}

Equations \ref{eq:chp2} and \ref{eq:chp3} model respectively the \textit{ramp up} and the \textit{ramp down} characteristic of a CHP profile. As has been said, this is a generic behaviour; in more realistic implementations these constraints should be modified accordingly to the real CHP system.

According to the literature (i.e. \cite{wang2015modelling}) usually CHP systems have a operational limits in terms of time to respect: for example they can't stay on all day long or viceversa they can't stay off for all the day. Equations \ref{eq:chp4} and \ref{eq:chp5} model this behavior.

\begin{equation}\label{eq:chp4}
    n_{CHP}^{on} \geq n_{CHP}^{on\_min}
\end{equation}
\begin{equation}\label{eq:chp5}
    n_{CHP}^{off} \geq n_{CHP}^{off\_min}
\end{equation}

with \(n_{CHP}^{on}\) and \(n_{CHP}^{off}\) representing the total number of timestamps in which, respectively, the CHP is on and off.

Also the cost related to the CHP needs to be modelled. Using only the diesel cost is not enough and also a startup cost \(c_{CHP}^{startup}\) should be considered in the model. The total CHP cost is calculated as:

\begin{equation}\label{eq:chp6}
    c_{CHP}(t) = c_{Diesel}(t)P_{CHP}(t)+c_{CHP}^{startup}CHP_{starting}(t)\;\;\;\;\;\;\forall t\in T
\end{equation}

where \(CHP_{starting}(t)\) is a binary variable that gets the value \(1\) if at the timestamp \(t\) the CHP is starting, \(0\) otherwise.

\subsection{Storage}\label{section:storage}

There are many papers that model in details the behaviour of batteries in a virtual power plant environment, like \cite{ju2017two, zhou2016optimal, xu2017factoring, bordin2017linear}. However, they are really specific to some kind of batteries, for example lithium-ion ones, and the purpose it to keep the model as general as possible.

That's why the model that will be proposed consider the \textit{State of Charge} (\(SoC\)) of the storage system, the \textit{efficiency} while charging and discharging (\(\eta_{c}\) and \(\eta_{d}\)), the \textit{maximum capacity} (\(max\_charge\)), the \textit{minimum capacity} (\(min\_charge\)) and the flow of energy that charge and discharge the battery (\(P_{St_{In}}\) and \(P_{St_{Out}}\)). The state of charge is modelled as a variable called \(charge(t)\).

\begin{equation}\label{eq:st1}
    charge(t) = charge(t-1)-\eta_{d}P_{St_{Out}}(t)+\eta_{c}P_{St_{In}}(t)\;\;\;\;\;\;\forall t\in T
\end{equation}

Equation \ref{eq:st1} models the main behaviour of the SoC of a storage system. However, also some bounds are necessary and can be found in equations \ref{eq:st2}, \ref{eq:st3} and \ref{eq:st4}.

\begin{equation}\label{eq:st2}
    min\_charge \leq charge(t) \leq max\_charge\;\;\;\;\;\;\forall t\in T
\end{equation}
\begin{equation}\label{eq:st3}
    0 \leq P_{St_{Out}}(t) \leq max\_delta\_discharge\;\;\;\;\;\;\forall t\in T
\end{equation}
\begin{equation}\label{eq:st4}
    0 \leq P_{St_{In}}(t) \leq max\_delta\_charge\;\;\;\;\;\;\forall t\in T
\end{equation}

\(max\_delta\_charge\) and \(max\_delta\_discharge\) are variables that represent the maximum quantity of energy that can respectively charge and discharge the storage. These values can be very different based on the number of timestamps and the type of the battery used.

The most challenging thing to model in a storage system is the associated cost to consider in the optimization. In fact, battery stress factors and degradation will force to replace the entire storage system, with a really big impact on the total operational cost of the virtual power plant.

The literature \cite{ju2017two, zhou2016optimal, xu2017factoring} gives very specific degradation models for different type of batteries, but to keep it at general as possible the cost is calculated with equation \ref{eq:st5} \cite{bordin2017linear}.

\begin{equation}\label{eq:st5}
    c_{St} = \frac{R}{L*E}
\end{equation}

\(R\) represent the battery replacement cost, \(L\) the lifetime throughput of the battery and \(E\) the squared root of the roundtrip efficiency of the battery.

\subsection{Grid}

The interaction with the grid can be modelled with two variables \(P_{Grid_{In}}\) and \(P_{Grid_{Out}}\) representing respectively the energy bought from the market and the energy sold to market. Based on literature \cite{bai2015optimal} and real data, bounds for the maximum and minimum net capacity are considered.

\begin{equation}\label{eq:grid1}
    P_{Grid_{In}}^{min} \leq P_{Grid_{In}}(t) \leq P_{Grid_{In}}^{max}\;\;\;\;\;\;\forall t\in T
\end{equation}
\begin{equation}\label{eq:grid2}
    P_{Grid_{Out}}^{min} \leq P_{Grid_{Out}}(t) \leq P_{Grid_{Out}}^{max}\;\;\;\;\;\;\forall t\in T
\end{equation}

\subsection{Load}

Given the projected load demand, the EMS can decide to shift the load to perform the cost optimization. The shifted load is given by:

\begin{equation}\label{eq:load1}
    \Tilde{P}_{Load}(t) = S_{Load}(t) + P_{Load}(t)\;\;\;\;\;\;\forall t\in T
\end{equation}

where \(\Tilde{P}_{Load}(t)\) represents the shifted load at the timestamp \(t\) and \(S_{Load}(t)\) is the amount of shift from the original load projection \(P_{Load}(t)\) at the timestamp \(t\).

The quantity of the shift is bounded in order to guarantee a reduction/increase of the demand, in each timestamp, by a maximum percentage of the original expected load (\(allowed\_shift\) in equations \ref{eq:load3} and \ref{eq:load4}).

\begin{equation}\label{eq:load2}
    S_{Load}^{Min}(t) \leq S_{Load}^{Max}(t) \leq S_{Load}(t)\;\;\;\;\;\;\forall t\in T
\end{equation}
\begin{equation}\label{eq:load3}
    S_{Load}^{Min}(t) = -P_{Load}(t)\frac{allowed\_shift}{100}
\end{equation}
\begin{equation}\label{eq:load4}
    S_{Load}^{Max}(t) = +P_{Load}(t)\frac{allowed\_shift}{100}
\end{equation}

It's also assumed that the total demand on the whole optimization horizon is constant. More specifically, the total requested demand has to stay unchanged also \textit{over multiple sub-periods of the horizon}. Formally, let \(T_{n}\) be the set of timestamps for the \(n\)-th sub-period, then the constraint can be formulated as:

\begin{equation}\label{eq:load5}
    \sum_{t\in T_{n}}S_{Load}(t) = 0
\end{equation}

\subsection{Power balance}

The virtual power plant must guarantee that the generated power is equal to the demanded load for each timestamp. At any point in time, the overall shifted load must be covered by an energy mix considering the generation from the internal sources, the storage system and the power bought from the energy market. Energy sold to the grid and stored in the batteries should be subtracted from the equation. Having said this, the power balance equation is:

\begin{equation}\label{eq:pb1}
    \begin{aligned}
        \Tilde{P}_{Load}(t) = P_{PV}(t)+P_{CHP}(t)+P_{Grid_{In}}(t)-P_{Grid_{Out}}(t)+P_{St_{Out}}(t)-P_{St_{In}}(t)\\\forall t\in T
    \end{aligned}
\end{equation}

\subsection{Objective function}

In this case, we consider as the objective of the EMS the minimization of the total operational costs \(c\) over the whole time horizon \(T\):

\begin{equation}\label{eq:obj1}
    \begin{aligned}
        c = \sum_{t\in T}c_{Grid_{In}}(t)P_{Grid_{In}}(t)+c_{CHP}(t)P_{CHP}(t)+\\
        +c_{St}(t)P_{St_{Out}}(t)-c_{Grid_{Out}}(t)P_{Grid_{Out}}(t)
    \end{aligned}
\end{equation}

\begin{equation}\label{eq:obj2}
    \begin{aligned}
        z = min(c)
    \end{aligned}
\end{equation}

where \(c_{Grid_{In}}(t)\) and \(c_{Grid_{Out}}(t)\) are respectively the price of the energy when it is bought and sold to the grid, \(c_{CHP}\) is the CHP cost calculated with equation \ref{eq:chp6} and \(c_{St}\) is the cost related to the storage (equation \ref{eq:st5}).
