import random
import time
from random import randint

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from p5_Aggregator_after_paper_25_03_2020.src.load import Load
from p5_Aggregator_after_paper_25_03_2020.src.profile import Profile
from p5_Aggregator_after_paper_25_03_2020.src.enums import ProfileType, LoadType
from p5_Aggregator_after_paper_25_03_2020.src.solver import Solver

sns.set()

random.seed(10)

pv_profiles = np.load('../Profili/processed_pv.npy')
load_profiles = np.load('../Profili/processed_aggregated_load.npy')
prices_profiles = np.load('../Profili/processed_prices.npy')

p = Profile(prices_profiles[11], ProfileType.PRICE)
pv = Profile(pv_profiles[11]*100000, ProfileType.PV)

millingmachine_Is = np.load('../Profili/millingmachine-I.npy')
millingmachine_IIs = np.load('../Profili/millingmachine-II.npy')

millingmachine_I = Profile(millingmachine_Is[11], ProfileType.LOAD)
millingmachine_II = Profile(millingmachine_IIs[11], ProfileType.LOAD)

def create_baseline(load, threshold, repeat, percentage = 0):
    result = load
    mean = np.mean(load)
    print(mean)
    for _ in range(repeat):
        for i in range(1, len(load)-1):
            diff = min(abs(result[i] - result[i - 1]), abs(result[i + 1] - result[i]))
            if diff > threshold:
                result[i] = (result[i-1]+result[i+1])/2
            if result[i] < mean:
                result[i] = result[i] + random.uniform(result[i], mean - result[i])
    result = [result[i] - percentage/100*result[i] for i in range(len(result))]
    return result

def aggregate_load_array(array, timestamps):
    result = [0] * timestamps
    for load in array:
        result = result + load.profile()
    return result

#baseline = [45000000]*millingmachine_I.timestamps

#loads = [millingmachine_I.profile, millingmachine_II.profile]
loads = [Load(millingmachine_Is[i], load_type=LoadType(randint(1, 4))) for i in range(len(millingmachine_Is))]
for l in loads:
    print(l.load_type)

baseline = create_baseline(aggregate_load_array(loads, 96), threshold=1, repeat=10, percentage=0)

data = {'n_timestamps': millingmachine_I.timestamps, 'n_loads': len(loads)}

start = time.time()
s = Solver(data)
end = time.time()
s.add_array_data_field('corrected_baseline', baseline)
s.add_array_data_field('pv_profile', pv.profile)
s.add_multidimension_array_data_field('load_profiles', loads, millingmachine_I.timestamps)
s.add_array_data_field('load_t1', [int(l.load_type == LoadType.T1) for l in loads])
s.add_array_data_field('load_t2', [int(l.load_type == LoadType.T2) for l in loads])
s.add_array_data_field('load_t3', [int(l.load_type == LoadType.T3) for l in loads])
s.add_array_data_field('load_t4', [int(l.load_type == LoadType.T4) for l in loads])
s.resolve(tee=True, pprint=True)

print('Optimal solution: %5.2f' %s.solution_value)

print(s.load_full_bool)

print(end-start)



fig, ax = plt.subplots(figsize=(20,15))
ax.set(xlabel='Timestamp (t)', ylabel='Active Power (MW)', title='Model Results')
plt.xticks(range(0, 96, 5))
#plt.plot(s.load_result[0], label='LR1')
#plt.plot(millingmachine_I.profile, label='L1')
ax.plot(baseline, label='Baseline')
for index, l in enumerate(s.load_result):
    ax.plot(l, label='LR'+str(index))
#plt.plot(millingmachine_II.profile, label='L2')
ax.plot(s.load_aggregated, label='Load')
#plt.plot(pv.profile, label='pv')
#plt.plot(s.grid_in, label='grid in')
#plt.plot(s.grid_out, label='grid out')
#plt.plot(millingmachine_I.profile+millingmachine_II.profile, label='Original load')
ax.plot(s.load_result_aggregated, label='Result load')
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

# for index, b in enumerate(s.load_full_bool):
#     fig, ax = plt.subplots(figsize=(20, 15))
#     plt.xticks(range(0, 96, 5))
#     ax.set(xlabel='Timestamp (t)', ylabel='Status', title='Load Status Result')
#     ax.plot(b, label='Load '+str(index))
#     plt.show()
# plt.legend()
# plt.show()
