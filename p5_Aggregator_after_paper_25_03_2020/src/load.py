from p5_Aggregator_after_paper_25_03_2020.src.enums import ProfileType
from p5_Aggregator_after_paper_25_03_2020.src.profile import Profile


class Load(object):

    def __init__(self, l, load_type,  timestamps=96):
        self.load_type = load_type
        self._profile = self.__setup_profile(l, timestamps)

    def profile(self):
        return self._profile.profile

    def append(self, value):
        self._profile.append(value)

    def __getitem__(self, item):
        return self._profile[item]

    def __setitem__(self, key, value):
        self._profile[key] = value

    def __repr__(self):
        return self._profile

    def __str__(self):
        return self._profile.__str__()

    def __setup_profile(self, l, t):
        return Profile(l, ProfileType.LOAD, t)