from enum import Enum

class ProfileType(Enum):
    LOAD = 1
    PV = 2
    CHP = 3
    STORAGE = 4
    PRICE = 5
    ZERO = 6
    NA = 7          # Not available, not specified

class LoadType(Enum):
    T1 = 1
    T2 = 2
    T3 = 3
    T4 = 4