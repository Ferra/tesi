import pyomo.environ as pyo
from pyomo.core import Var, Objective
from pyomo.opt import SolverStatus, TerminationCondition
from Virtus_Tesi.model import DispatcherModel


class Solver:

    def __init__(self, data):
        self.data = data
        self.pods = self.data[None]['n_pods'][None]
        self.timestamps = self.data[None]['n_timestamps'][None]
        self.shifting_bool = False
        self.shifting_intervals_check()
        #self.powers_result = [[0 for x in range(self.timestamps)] for y in range(self.pods)]
        self.load_shifted_result = [[0 for _ in range(self.timestamps)] for _ in range(self.pods)]
        self.grid_out_result = [[0 for _ in range(self.timestamps)] for _ in range(self.pods)]
        self.grid_in_result = [[0 for _ in range(self.timestamps)] for _ in range(self.pods)]
        self.chp_result = [[0 for _ in range(self.timestamps)] for _ in range(self.pods)]
        self.solution_value = None # For the solution value
        self.model = DispatcherModel(self.data)


    def resolve(self, tee=True, pprint=False):
        instance = self.model.create_instance()
        #solver = pyo.SolverFactory('gurobi', solver_io="python")
        solver = pyo.SolverFactory('cplex', executable='/Applications/CPLEX_Studio129/cplex/bin/x86-64_osx/cplex')
        #solver = pyo.SolverFactory('ipopt')
        #solver.options['optimalitytarget'] = 3
        result = solver.solve(instance, tee=tee)

        if pprint:
            instance.pprint()

        # Check the results and extract the values if the solution is optimal
        if (result.solver.status == SolverStatus.ok) and (
                result.solver.termination_condition == TerminationCondition.optimal):

            # instance.pprint()

            for v in instance.component_objects(Var, active=True):
                # print ("Variable",v)
                varobject = getattr(instance, str(v))
                if v.name == 'load_shifted':
                    for index in varobject:
                        self.load_shifted_result[index[0]][index[1]] = varobject[index].value
                if v.name == 'grid_out':
                    for index in varobject:
                        self.grid_out_result[index[0]][index[1]] = varobject[index].value
                if v.name == 'grid_in':
                    for index in varobject:
                        self.grid_in_result[index[0]][index[1]] = varobject[index].value
                if v.name == 'chp_step_final_power':
                    for index in varobject:
                        self.chp_result[index[0]][index[1]] = varobject[index].value

            for v in instance.component_objects(Objective, active=True):
                self.solution_value = v.expr()
            #print("%.2f" % self.solution_value)

            #return self

        elif (result.solver.termination_condition == TerminationCondition.infeasible):
            print('############################ INFEASIBLE MODEL ############################')
            raise Exception('############################ INFEASIBLE MODEL ############################')
        else:
            print('############################ SOMETHING WENT WRONG ############################')
            print("Solver Status: ", result.solver.status)
            raise Exception(
                "############################ SOMETHING WENT WRONG ############################\nSolver Status: ",
                result.solver.status)

    def shifting_intervals_check(self):
        if self.data[None]['n_shifting_intervals'][None] > 0:
            self.data[None].update({'shifting_bool': {None: 1}})
            self.shifting_bool = True
        elif self.data[None]['n_shifting_intervals'][None] == 0:
            self.data[None].update({'shifting_bool': {None: 0}})
            self.data[None].update({'n_shifting_intervals': {None: 1}})
        else:
            raise Exception('n_shifting_intervals < 0')

        if self.timestamps % self.data[None]['n_shifting_intervals'][None] != 0:
            raise Exception('n_timestamp MUST be multiple of n_shifting_intervals')

    def shifting_bool(self):
        return self.shifting_bool

    def chp_result(self):
        return self.chp_result

    def load_shifted_result(self):
        return self.load_shifted_result

    def grid_out_result(self):
        return self.grid_out_result

    def grid_in_result(self):
        return self.grid_in_result

    def solution_value(self):
        return self.solution_value
