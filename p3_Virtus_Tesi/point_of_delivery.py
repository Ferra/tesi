import random
from enum import Enum
import numpy as np
import matplotlib.pyplot as plt

class ProfileType(Enum):
    LOAD = 1
    PV = 2
    CHP = 3
    ZERO = 4


class CHPParameters:

    def __init__(self, chp_max=100, chp_min_step_up=5, chp_min_step_down=5, chp_ramp_up_limit=90,
                 chp_ramp_down_limit=90):
        self.chp_max = chp_max
        self.chp_min_step_up = chp_min_step_up
        self.chp_min_step_down = chp_min_step_down
        self.chp_ramp_up_limit = chp_ramp_up_limit
        self.chp_ramp_down_limit = chp_ramp_down_limit

    def randomize(self):
        self.chp_max = self.chp_max + (-1)**random.randint(0, 100) * random.randrange(20, 100, 10)
        self.chp_min_step_up = self.chp_min_step_up + (-1) ** random.randint(0, 100) * random.randrange(0, 4, 1)
        self.chp_min_step_down = self.chp_min_step_down + (-1) ** random.randint(0, 100) * random.randrange(0, 4, 1)
        return self


class Profile:

    def __init__(self, profile_type, timestamps=96):
        self.timestamps = timestamps
        self.type = profile_type
        self.profile = [0] * timestamps
        self.init_profile()

    def init_profile(self):
        if self.type == ProfileType.LOAD:
            self.init_load()
        elif self.type == ProfileType.PV:
            self.init_pv()
        elif self.type == ProfileType.CHP:
            self.init_chp()
        else:
            return

    def init_load(self):
        load = np.load('Realizations_Load.npy')

        load_h = load[0] + 20 / 100 * load[0]
        load_l = load[0] - 20 / 100 * load[0]

        for i in range(self.timestamps):

            r = random.random()
            if r < 0.1:
                scale = 1.5
            elif r > 0.9:
                scale = 0.5
            else:
                scale = 1

            self.profile[i] = random.uniform(load_l[i] * scale, load_h[i] * scale)

    def init_pv(self):
        pv = np.load('Realizations_PV.npy')

        pv_h = pv[0] + 5 / 100 * pv[0]
        pv_l = pv[0] - 5 / 100 * pv[0]

        for i in range(self.timestamps):

            r = random.random()
            if r < 0.2:
                scale = 1.5
            elif r > 0.8:
                scale = 0.5
            else:
                scale = 1
            # Multiplied by -1 because pv is negative by convention
            self.profile[i] = (-1) * random.uniform(pv_l[i] * scale, pv_h[i] * scale)

    def init_chp(self):
        return


class PointOfDelivery:

    load_profile: Profile
    pv_profile: Profile
    chp_profile: Profile
    chp_parameters: CHPParameters
    timestamps: int

    def __init__(self, has_load, has_pv, has_chp, timestamps=96, chp_parameters = CHPParameters()):
        self.has_load = has_load
        self.has_pv = has_pv
        self.has_chp = has_chp
        self.chp_parameters = chp_parameters
        self.timestamps = timestamps
        self.pod_init()

    def pod_init(self):
        if self.has_load:
            self.load_profile = Profile(ProfileType.LOAD, self.timestamps)
        else:
            self.load_profile = Profile(ProfileType.ZERO, self.timestamps)
        if self.has_pv:
            self.pv_profile = Profile(ProfileType.PV, self.timestamps)
        else:
            self.pv_profile = Profile(ProfileType.ZERO, self.timestamps)
        if self.has_chp:
            self.chp_profile = Profile(ProfileType.CHP, self.timestamps)
        else:
            self.chp_profile = Profile(ProfileType.ZERO, self.timestamps)

    def plot(self):
        if self.has_load:
            plt.plot(self.load_profile.profile, label="Load")
        if self.has_pv:
            plt.plot(self.pv_profile.profile, label="PV")
        if self.has_chp:
            plt.plot(self.chp_profile.profile, label="CHP")
        plt.legend()
        plt.show()