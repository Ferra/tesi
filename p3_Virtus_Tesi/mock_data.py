import random

from p3_Virtus_Tesi.point_of_delivery import PointOfDelivery, CHPParameters

def random_point_of_delivery(n_pods):
    pods = []
    for i in range(n_pods):
        l = bool(random.getrandbits(1))
        p = bool(random.getrandbits(1))
        c = bool(random.getrandbits(1))
        while not l and not p and not c:
            l = bool(random.getrandbits(1))
            p = bool(random.getrandbits(1))
            c = bool(random.getrandbits(1))
        if c:
            pods.append(PointOfDelivery(l, p, c, CHPParameters().randomize()))
        else:
            pods.append(PointOfDelivery(l, p, c))
        pods[i].plot()

def incoming_message_mock_values(n_pods, n_timestamps, n_shifting_intervals = 1):
    data = {None: {}}
    data[None]['n_timestamps'] = {None: n_timestamps}
    data[None]['n_pods'] = {None: n_pods}
    data[None]['n_shifting_intervals'] = {None: n_shifting_intervals}
    data[None]['load_shifting_bound'] = {}
    data[None]['load_predicted'] = {}
    data[None]['load_uncertainty'] = {}
    data[None]['pv_predicted'] = {}
    data[None]['pv_uncertainty'] = {}
    data[None]['grid_in_cost'] = {}
    data[None]['grid_out_cost'] = {}
    in_cost = 1.30
    out_cost = 0.90
    for i in range(n_pods):
        # Shift_bound constant for each pod (%)
        shift_bound = 20 + (-1) ** random.randint(0, 100) * random.randrange(0, 10, 1)
        for j in range(n_timestamps):
            # For shifting_bound not constant at each timestamp use the following row
            # data[None]['load_shifting_bounds'][(i, j)] = 10 + (-1)**random.randint(0, 100) * random.randrange(0, 10, 1)
            data[None]['load_shifting_bound'][(i, j)] = shift_bound
            data[None]['load_predicted'][(i, j)] = 200 + (-1)**random.randint(0, 100) * random.randrange(20, 100, 10)
            data[None]['load_uncertainty'][(i, j)] = 50 + (-1) ** random.randint(0, 100) * random.randrange(0, 20, 2)
            data[None]['pv_predicted'][(i, j)] = -180 + (-1) ** random.randint(0, 100) * random.randrange(20, 100, 10)
            data[None]['pv_uncertainty'][(i, j)] = 50 + (-1) ** random.randint(0, 100) * random.randrange(0, 20, 2)
            data[None]['grid_in_cost'][(i, j)] = in_cost
            data[None]['grid_out_cost'][(i, j)] = out_cost
    #print(data)
    return data


def incoming_message(list_of_pods: [PointOfDelivery], n_shifting_intervals = 1):
    n_pods = len(list_of_pods)
    if n_pods > 0:
        n_timestamps = list_of_pods[0].timestamps
        data = {None: {}}
        data[None]['n_timestamps'] = {None: n_timestamps}
        data[None]['n_pods'] = {None: n_pods}
        data[None]['n_shifting_intervals'] = {None: n_shifting_intervals}
        data[None]['load_shifting_bound'] = {}
        data[None]['load_predicted'] = {}
        data[None]['load_uncertainty'] = {}
        data[None]['pv_predicted'] = {}
        data[None]['pv_uncertainty'] = {}
        data[None]['grid_in_cost'] = {}
        data[None]['grid_out_cost'] = {}
        data[None]['chp_min_step_up'] = {}
        data[None]['chp_min_step_down'] = {}
        data[None]['chp_ramp_up_limit'] = {}
        data[None]['chp_ramp_down_limit'] = {}
        data[None]['diesel_cost'] = {}
        in_cost = 1.30
        out_cost = 0.90
        diesel_cost = 1.2
        for i in range(n_pods):
            # Shift_bound constant for each pod
            shift_bound = 10 + (-1) ** random.randint(0, 100) * random.randrange(0, 10, 1)
            data[None]['chp_min_step_up'][i] = list_of_pods[i].chp_parameters.chp_min_step_up
            data[None]['chp_min_step_down'][i] = list_of_pods[i].chp_parameters.chp_min_step_down
            data[None]['chp_ramp_up_limit'][i] = list_of_pods[i].chp_parameters.chp_ramp_up_limit
            data[None]['chp_ramp_down_limit'][i] = list_of_pods[i].chp_parameters.chp_ramp_down_limit
            for j in range(n_timestamps):
                # For shifting_bound not constant at each timestamp use the following row
                # data[None]['load_shifting_bounds'][(i, j)] = 10 + (-1)**random.randint(0, 100) * random.randrange(0, 10, 1)
                data[None]['load_shifting_bound'][(i, j)] = shift_bound
                data[None]['load_predicted'][(i, j)] = list_of_pods[i].load_profile.profile[j]
                data[None]['load_uncertainty'][(i, j)] = 50 + (-1) ** random.randint(0, 100) * random.randrange(0, 20, 2)
                data[None]['pv_predicted'][(i, j)] = list_of_pods[i].pv_profile.profile[j]
                data[None]['pv_uncertainty'][(i, j)] = 50 + (-1) ** random.randint(0, 100) * random.randrange(0, 20, 2)
                data[None]['grid_in_cost'][(i, j)] = in_cost
                data[None]['grid_out_cost'][(i, j)] = out_cost
                data[None]['diesel_cost'][(i, j)] = diesel_cost
        #print(data)
        return data
    else:
        return Exception('n_pods <= 0')