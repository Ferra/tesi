import pyomo.environ as pyo

class DispatcherModel :

    model = pyo.AbstractModel()

    def __init__(self, data):
        self.data = self.__data_dictionary_creation(data)
        self.__model_superparameters_setup()

    def __data_dictionary_creation(self, data):
        # TODO: parse data object from BDE
        # The parameter 'data' should be the message from the BDE. Here it should be parsed to match the following form:
        # { None: {
        #   'n_pods': { None: 100 },
        #   'n_flex_param': { None: 3 },
        #   'n_flex_max': { None: 3 },
        #   'baselines': { (0,0): 0.2, (0,1): 0.4, ... }
        #   'flexibility_bands': {
        #       (0,0,0): 0.2, (0,0,1): 100, (0,0,2): 200, (0,1,0): 0.15, ...
        #    }
        #   }
        # }

        #print(data)
        return data


    def __model_superparameters_setup(self):

        # Number of timestamps
        self.model.n_timestamps = pyo.Param(domain=pyo.PositiveIntegers)

        # Number of pods
        self.model.n_pods = pyo.Param(domain=pyo.PositiveIntegers)

        # Number of shifting intervals
        self.model.n_shifting_intervals = pyo.Param(domain=pyo.PositiveIntegers)


    def __model_parameters_setup(self):

        self.model.T = pyo.RangeSet(0, self.model.n_timestamps - 1)
        self.model.P = pyo.RangeSet(0, self.model.n_pods - 1)
        self.model.S = pyo.RangeSet(0, self.model.n_shifting_intervals - 1)

        ######################################################
        #
        # LOAD
        #
        ######################################################

        # Predicted load at each timestamp for each pod (positive for convention)
        self.model.load_predicted = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeReals)
        # Uncertainty on the load at each timestamp for each pod (%)
        self.model.load_uncertainty = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeIntegers)
        # Shifting bound of the load (%) - Valid above and under the predicted load
        self.model.load_shifting_bound = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeReals)
        # Boolean var - 1: shifting admitted, 0: shifting not admitted
        self.model.shifting_bool = pyo.Param(domain=pyo.Binary)

        ######################################################
        #
        # PV
        #
        ######################################################

        # Predicted PV power at each timestamps for each pod (negative for convention)
        self.model.pv_predicted = pyo.Param(self.model.P, self.model.T, domain=pyo.NonPositiveReals)
        # Uncertainty on the PV power at each timestamp for each pod (%)
        self.model.pv_uncertainty = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeIntegers)

        ######################################################
        #
        # GRID
        #
        ######################################################

        # Cost of the energy that has to be buyed from the grid (€/MW)
        self.model.grid_in_cost = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeReals)

        # Cost of the energy that has to be sold to the grid (€/MW)
        self.model.grid_out_cost = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeReals)

        ######################################################
        #
        # CHP
        #
        ######################################################

        # Chp max power
        self.model.chp_max = pyo.Param(self.model.P, domain=pyo.NonNegativeReals, default=300, mutable=True)

        # Chp min power
        self.model.chp_min = pyo.Param(self.model.P, domain=pyo.NonNegativeReals, default=0, mutable=True)

        # Minimum number of timestamps in which the chp must be on
        self.model.chp_min_step_up = pyo.Param(self.model.P, domain=pyo.NonNegativeIntegers)

        # Minimum number of timestamps in which the chp must be off
        self.model.chp_min_step_down = pyo.Param(self.model.P, domain=pyo.NonNegativeIntegers)

        # Maximum chp throughput rise in a timestamp step
        self.model.chp_ramp_up_limit = pyo.Param(self.model.P, domain=pyo.NonNegativeReals)

        # Maximum chp throughput fall in a timestamp step
        self.model.chp_ramp_down_limit = pyo.Param(self.model.P, domain=pyo.NonNegativeReals)

        # Cost of starting chp
        self.model.chp_startup_cost = pyo.Param(self.model.P, domain=pyo.NonNegativeReals, default=0, mutable=True)

        # Cost of shutting down the chp
        self.model.chp_shutdown_cost = pyo.Param(self.model.P, domain=pyo.NonNegativeReals, default=0, mutable=True)

        # Cost of the diesel (€/MW)
        self.model.diesel_cost = pyo.Param(self.model.P, self.model.T, domain=pyo.NonNegativeReals)

        # Chp power at timestamp t=0
        self.model.chp_initial_output_power = pyo.Param(self.model.P, domain=pyo.NonNegativeReals, default=0, mutable=True)

        # Chp status at timestamp t=0 - 0: down, 1: up
        self.model.chp_initial_status = pyo.Param(self.model.P, domain=pyo.Binary, default=0, mutable=True)


    def __model_variable_setup(self):

        # For testing
        #self.model.var = pyo.Var(self.model.P, self.model.T, domain=pyo.Binary)

        # Quantity of load shifted for each pod at each timestamp
        self.model.load_shifted = pyo.Var(self.model.P, self.model.T, domain=pyo.Reals)

        # Quantity of energy sold to the grid for each pod ad each timestamp
        self.model.grid_out = pyo.Var(self.model.P, self.model.T, domain=pyo.NonNegativeReals)

        # Quantity of energy buy from the grid for each pod at each timestamp
        self.model.grid_in = pyo.Var(self.model.P, self.model.T, domain=pyo.NonNegativeReals)

        # Chp variables
        self.model.chp = pyo.Var(self.model.P, self.model.T, domain=pyo.NonNegativeReals)
        self.model.chp_status = pyo.Var(self.model.P, self.model.T, domain=pyo.Binary)  # 0: down, 1: up
        self.model.chp_starting_up = pyo.Var(self.model.P, self.model.T, domain=pyo.Binary)
        self.model.chp_shutting_down = pyo.Var(self.model.P, self.model.T, domain=pyo.Binary)
        self.model.chp_total_cost = pyo.Var(self.model.P, self.model.T, domain=pyo.NonNegativeReals)
        self.model.chp_step_initial_power = pyo.Var(self.model.P, self.model.T, domain=pyo.NonNegativeReals)
        self.model.chp_step_final_power = pyo.Var(self.model.P, self.model.T, domain=pyo.NonNegativeReals)

    def __model_constraint_setup(self):

        # For testing
        #def const(m, p):
        #    return sum(m.var[p, t] for t in m.T) <= 10
        #self.model.const = pyo.Constraint(self.model.P, rule=const)

        # Upper bound definition of the shifted load
        def load_shift_ub(m, p, t):
            return m.load_shifted[p, t] <= m.shifting_bool*m.load_predicted[p, t]*m.load_shifting_bound[p, t]/100
        self.model.load_shift_ub = pyo.Constraint(self.model.P, self.model.T, rule=load_shift_ub)

        # Lower bound definition of the shifted load
        def load_shift_lb(m, p, t):
            return m.load_shifted[p, t] >= m.shifting_bool*(-1)*(m.load_predicted[p, t]*m.load_shifting_bound[p, t]/100)
        self.model.load_shift_lb = pyo.Constraint(self.model.P, self.model.T, rule=load_shift_lb)

        # Rule of the load shift (sum in Tn = 0)
        def load_shifting_rule(m, p, s):
            # TODO: n_timestamps must be multiple of n_shifting_intervals. Check it when data arrive.
            r = int(pyo.value(m.n_timestamps)/pyo.value(m.n_shifting_intervals))
            return sum(m.load_shifted[p, t] for t in range(r * s, r * (s + 1))) == 0
        self.model.load_shifting_rule = pyo.Constraint(self.model.P, self.model.S, rule=load_shifting_rule)

        # Power balance
        def power_balance(m, p, t):
            return m.load_predicted[p, t] + m.load_shifted[p,t] + m.pv_predicted[p, t] + m.grid_out[p, t] - m.grid_in[p, t] - m.chp[p, t] == 0
        self.model.power_balance = pyo.Constraint(self.model.P, self.model.T, rule=power_balance)

        ######################################################
        # Chp constraints

        def chp_lb(m, p, t):
            return m.chp_step_final_power[p, t] >= m.chp_min[p]
        self.model.chp_lb = pyo.Constraint(self.model.P, self.model.T, rule=chp_lb)

        def chp_ub(m, p, t):
            return m.chp_step_final_power[p, t] <= m.chp_max[p]
        self.model.chp_ub = pyo.Constraint(self.model.P, self.model.T, rule=chp_ub)

        def chp_ramp_up(m, p, t):
            return m.chp_step_final_power[p, t] - m.chp_step_initial_power[p, t] <= m.chp_ramp_up_limit[p]
        self.model.chp_ramp_up = pyo.Constraint(self.model.P, self.model.T, rule=chp_ramp_up)

        def chp_ramp_down(m, p, t):
            return m.chp_step_initial_power[p, t] - m.chp_step_final_power[p, t] <= m.chp_ramp_down_limit[p]
        self.model.chp_ramp_down = pyo.Constraint(self.model.P, self.model.T, rule=chp_ramp_down)

        def chp_step_initial_power_definition(m, p, t):
            if t == 0:
                return m.chp_step_initial_power[p, t] == m.chp_initial_output_power[p]
            else:
                return m.chp_step_initial_power[p, t] == m.chp_step_final_power[p, t - 1]
        self.model.chp_step_initial_power_definition = pyo.Constraint(self.model.P, self.model.T, rule=chp_step_initial_power_definition)

        def chp_current_definition(m, p, t):
            return m.chp[p, t] == (m.chp_step_initial_power[p, t] + m.chp_step_final_power[p, t]) / 2
        self.model.chp_current_definition = pyo.Constraint(self.model.P, self.model.T, rule=chp_current_definition)

        def chp_min_step_up_constraint(m, p):
            return sum(m.chp_status[p, t] for t in m.T) >= m.chp_min_step_up[p]
        self.model.chp_min_step_up_constraint = pyo.Constraint(self.model.P, rule=chp_min_step_up_constraint)

        def chp_min_step_down_constraint(m, p):
            return sum(1 - m.chp_status[p, t] for t in m.T) >= m.chp_min_step_down[p]
        self.model.chp_min_step_down_constraint = pyo.Constraint(self.model.P, rule=chp_min_step_down_constraint)

        def chp_status_definition(m, p, t):
            return (0, -m.chp[p, t] + m.chp_max[p] * m.chp_status[p, t], m.chp_max[p] - 1)
            # return m.chp_status[t] == boolm.chp[t]
        self.model.chp_status_definition = pyo.Constraint(self.model.P, self.model.T, rule=chp_status_definition)

        def chp_starting_up_definition(m, p, t):
            if t == 0:
                return m.chp_starting_up[p, t] >= m.chp_status[p, t] - m.chp_initial_status[p]
            else:
                return m.chp_starting_up[p, t] >= m.chp_status[p, t] - m.chp_status[p, t - 1]
        self.model.chp_starting_up_definition = pyo.Constraint(self.model.P, self.model.T, rule=chp_starting_up_definition)

        ######################################################

    def __model_obj_func_setup(self):

        # Objective function for testing
        # def obj(m):
        #     return sum(m.var[p, t]*m.load_predicted[p, t] for p in m.P for t in m.T)
        # self.model.obj = pyo.Objective(sense=pyo.maximize, rule=obj)

        # Objective function to miminize the use of the grid
        # def obj_function_minimum_grid(m):
        #     return
        # self.model.obj_function_minimum_grid = pyo.Objective(sense=pyo.minimize, rule=obj_function_minimum_grid)

        # Objective function to miminize the costs
        def obj_function_minimum_costs(m):
            return sum(m.grid_in_cost[p, t] * m.grid_in[p, t] - m.grid_out_cost[p, t] * m.grid_out[p, t] + m.diesel_cost[p, t]*m.chp[p, t] + m.chp_starting_up[p, t]*m.chp_startup_cost[p] for t in m.T for p in m.P)
        self.model.obj_function_minimum_costs = pyo.Objective(sense=pyo.minimize, rule=obj_function_minimum_costs)


    # Method to create model instance with previous passed data directly from the solver class
    def create_instance(self):
        self.__model_parameters_setup()
        self.__model_variable_setup()
        self.__model_constraint_setup()
        self.__model_obj_func_setup()
        return self.model.create_instance(self.data)