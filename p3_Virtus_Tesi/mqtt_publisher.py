# Simulate the publish of mqtt messages

import paho.mqtt.client as mqtt
from p3_Virtus_Tesi import mock_data

def mqtt_on_publish(client, userdata, mid):
    print("Messaggio pubblicato")

def mqtt_on_connect(client, userdata, flags, rc):
    print("Connesso con successo")

mqtt_address = "localhost"

mqtt_client = mqtt.Client("test_publisher")
mqtt_client.on_connect = mqtt_on_connect
mqtt_client.on_publish = mqtt_on_publish
mqtt_client.connect(mqtt_address)


data = mock_data.incoming_message(10, 24)
mqtt_client.publish("topic/mock", str(data))