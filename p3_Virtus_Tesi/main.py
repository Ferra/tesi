from Virtus_Tesi.point_of_delivery import PointOfDelivery, CHPParameters
from Virtus_Tesi.solver import Solver
from Virtus_Tesi import mock_data
import matplotlib.pyplot as plt
import random
import numpy as np
import paho.mqtt.client as mqtt

#######################################################################
# def solve_model(data):
#     s = Solver(data)
#     s.resolve()
#     print(s.result)
#     print(s.solution_value)
#
# def mqtt_on_message(client, userdata, message):
#     print("Messaggio ricevuto")
#     data = message.payload.decode("utf-8")
#     solve_model(data)
#
# def mqtt_on_connect(client, userdata, flags, rc):
#     print("Connesso con successo")
#     mqtt_client.subscribe("topic/mock")
#
# mqtt_address = "localhost"
#
# mqtt_client = mqtt.Client("test_client")
# mqtt_client.on_connect = mqtt_on_connect
# mqtt_client.on_message = mqtt_on_message
#
# mqtt_client.connect(mqtt_address)
# mqtt_client.loop_forever()
#######################################################################

random.seed(401)
n_timestamps = 96

#data = mock_data.incoming_message_mock_values(n_pods=1, n_timestamps=12, n_shifting_intervals=1)
pods = [PointOfDelivery(1, 1, 0, timestamps=n_timestamps, chp_parameters=CHPParameters().randomize())]
data = mock_data.incoming_message(pods, n_shifting_intervals=0)
s = Solver(data)
s.resolve(tee=False, pprint=True)

print('Optimal solution: %5.2f' %s.solution_value)

for i in range(len(pods)):
    plt.plot(pods[i].load_profile.profile, label='Load')
    if s.shifting_bool:
        plt.plot([x+y for x, y in zip(pods[i].load_profile.profile, s.load_shifted_result[i])], label='Load shifted')
    plt.plot([(-1)*x for x in pods[i].pv_profile.profile], label='PV')
    plt.plot(s.grid_in_result[i], label='Grid in')
    plt.plot(s.grid_out_result[i], label='Grid out')
    plt.plot(s.chp_result[i], label='CHP')
    plt.legend()
    plt.show()
